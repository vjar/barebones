#pragma once
#include "xkbcommon-keysyms.h"

typedef struct persist_state {
	enum {
		ENGINE_EXIT_QUIT,
		ENGINE_EXIT_RELOAD,
	} engine_exit_reason;
	void *engine_state;
} persist_state_t;

#define INPUTS_FORMAT(name, value) \
	INPUT_##name = value,
enum input_id {
	INPUTS_LIST
};
#undef INPUTS_FORMAT

typedef struct input_state {
	_Bool direction_down;
	enum input_id input;
} input_state_t;

#define PFN_LOADER_LIST \
	PFN_FORMAT(void*, loader_mem, (const uint64_t len), loader_mem) \
	PFN_FORMAT(void, loader_printf, (const char *fmt, ...), printf) \
	PFN_FORMAT(void*, loader_vulkan_sym, (const char *name), loader_vulkan_sym) \
	PFN_FORMAT(void*, loader_memset, (void *area, int constant_byte, uint64_t len), memset) \
	PFN_FORMAT(void*, loader_memcpy, (void *restrict dst, const void *restrict src, uint64_t len), memcpy) \
	PFN_FORMAT(uint64_t, loader_strlen, (const char *s), strlen) \
	PFN_FORMAT(void, loader_timestamp, (const uint32_t *t_sec, const uint32_t *t_nsec), loader_timestamp) \
	PFN_FORMAT(void, loader_wayland_display_dispatch_pending, (void), loader_wayland_display_dispatch_pending) \
	PFN_FORMAT(struct wl_display *, loader_wayland_display_get, (void), loader_wayland_display_get) \
	PFN_FORMAT(struct wl_surface *, loader_wayland_surface_get, (void), loader_wayland_surface_get) \
	PFN_FORMAT(const double *, loader_pointer_position, (void), loader_pointer_position) \
	PFN_FORMAT(int, loader_dlib_update_available, (void), loader_dlib_update_available) \
	PFN_FORMAT(int, loader_file_read, (char *buf, uint64_t *bufsz, const char *filename), loader_file_read) \
	PFN_FORMAT(uint64_t, loader_inputs, (char *buf, const uint64_t bufsz), loader_inputs) \
	PFN_FORMAT(void, loader_mem_free, (void* addr, uint64_t len), munmap)

#define PFN_FORMAT(type, name, args, loadername) PFN_##name,
enum PFN_LOADER {
	PFN_LOADER_LIST
};
#undef PFN_FORMAT

// loader pfn typedefs
typedef void* (*pfn_loader_addr)(const enum PFN_LOADER which);
#define PFN_FORMAT(type, name, args, loadername) \
	typedef type (*pfn_##name) args;
PFN_LOADER_LIST
#undef PFN_FORMAT

// example unwrapping:
//#undef pfn_loader_memcpy
//typedef void* (*pfn_loader_memcpy)(void *restrict dst, const void *restrict src, uint64_t len);

// engine pfn typedef
// currently the loader only calls one function
typedef int (*pfn_engine_entrypoint)(persist_state_t *persist_state, pfn_loader_addr loader_addr);
