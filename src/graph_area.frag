#version 450
layout(location = 0) out vec4 outColor;
in vec4 gl_FragCoord;
void main() {
	float y = gl_FragCoord.y / 600.0;
	outColor = vec4(y > 0.5 ? -y + 1.0 : 0.0, y < 0.5 ? y : 0.0, 0.0, 1.0);
}
