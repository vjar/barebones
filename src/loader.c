#define _DEFAULT_SOURCE
#include <stdint.h>
#include "loader.h"
#ifdef __linux__
#include <stdlib.h>
#include <sys/mman.h>
#include <dlfcn.h>
#include <stdio.h> // printf
#include <string.h> // memset
#include <time.h> // clock_gettime
#include <sys/stat.h>
#include <wayland-client-protocol.h>
#include "xdg-shell-client-protocol.h"

#include <unistd.h>
#include <errno.h>
#include <sys/inotify.h>
#include <assert.h>

#include <linux/input-event-codes.h>
#include <xkbcommon/xkbcommon.h>

static const char engine_dlib[] = "engine.so";
static const char vulkan_dlib[] = "libvulkan.so.1";
static int fd_inotify = 0;
#endif

typedef struct window_context {
	struct wl_display *display;
	struct wl_registry *registry;
	struct wl_compositor *compositor;
	struct xdg_wm_base *xdg_wm_base;
	struct wl_surface *surface;
	struct wl_registry_listener registry_listener;
	struct xdg_surface *xdg_surface;
	struct xdg_toplevel *xdg_toplevel;
	struct xdg_wm_base_listener xdg_wm_base_listener;
	struct xdg_surface_listener xdg_surface_listener;
	struct wl_pointer_listener pointer_listener;
	struct wl_keyboard_listener keyboard_listener;
	struct wl_seat *seat;
	struct wl_pointer *pointer;
	struct xkb_context *xkb_context;
	struct wl_keyboard *keyboard;
	struct xkb_keymap *keymap;
	struct xkb_state *xkb_state;
	double pointer_position[2];
	uint64_t ninputs;
	input_state_t inputs_buf[8];
} window_context_t;

static window_context_t ctx_window = { 0 };
static void *hn_vulkan_dl = 0;

void *loader_mem(const uint64_t length) {
	return mmap(NULL,
		length,
		PROT_READ | PROT_WRITE,
		MAP_PRIVATE | MAP_ANONYMOUS,
		-1,
		0
	);
}

void *loader_vulkan_sym(const char* name) {
	return dlsym(hn_vulkan_dl, name);
}

void loader_timestamp(uint32_t *t_sec, uint32_t *t_nsec) {
	struct timespec time;
	clock_gettime(CLOCK_REALTIME, &time);
	*t_sec = time.tv_sec;
	*t_nsec = time.tv_nsec;
}

void loader_wayland_display_dispatch_pending() {
	wl_display_dispatch_pending(ctx_window.display);
}

struct wl_display *loader_wayland_display_get() {
	return ctx_window.display;
}

struct wl_surface *loader_wayland_surface_get() {
	return ctx_window.surface;
}

double *loader_pointer_position() {
	return ctx_window.pointer_position;
}

int loader_dlib_update_available() {
	char buf[4096] __attribute__((aligned((__alignof__(struct inotify_event)))));
	ssize_t nbytes = read(fd_inotify, buf, sizeof(buf));
	const struct inotify_event *ev;
	for (char *p = buf; p < buf + nbytes; p += sizeof(*ev) + ev->len) {
		ev = (struct inotify_event *)p;
		if (!(ev->mask & IN_CLOSE_WRITE)) { continue; }
		if (strncmp(ev->name, engine_dlib, ev->len) == 0) {
			return 1;
		}
	}

	return 0;
}

int loader_file_read(char *buf, uint64_t *bufsz, const char *filename) {
	if (!buf) {
		struct stat s;
		if (stat(filename, &s) != 0) { return 1; }
		*bufsz = (uint64_t)s.st_size;
	} else {
		FILE *hn = fopen(filename, "rb");
		if (!hn) { return 1; }
		uint64_t nbytes = fread(buf, sizeof(*buf), *bufsz, hn);
		fclose(hn);
		if (nbytes != *bufsz) { return 1; }
	}
	return 0;
}

uint64_t loader_inputs(char *buf, const uint64_t bufsz) {
	const uint64_t nbytes = bufsz > ctx_window.ninputs * sizeof(*ctx_window.inputs_buf) ? ctx_window.ninputs * sizeof(*ctx_window.inputs_buf) : bufsz;
	memcpy(buf, ctx_window.inputs_buf, nbytes);

	ctx_window.ninputs = 0;
	return nbytes;
}

#define PFN_FORMAT(type, name, args, loadername) \
	case PFN_##name: \
		return (void*)loadername;
void *loader_addr(const enum PFN_LOADER which) {
	switch (which) {
		PFN_LOADER_LIST
		default:
			return 0;
	};
}
#undef PFN_FORMAT

static int loader_prepare(persist_state_t *persist_state) {
	memset(persist_state, 0, sizeof(*persist_state));
	hn_vulkan_dl = dlopen(vulkan_dlib, RTLD_LAZY);
	if (!hn_vulkan_dl) { return 1; }

	fd_inotify = inotify_init1(IN_NONBLOCK);
	inotify_add_watch(fd_inotify, ".", IN_CLOSE_WRITE | IN_ONLYDIR);
	return 0;
}

static int loader_engine_load(persist_state_t *persist_state) {
	void *hn = dlopen(engine_dlib, RTLD_LAZY); // -rpath=.
	if (!hn) { return 1; }

	pfn_engine_entrypoint engine_entrypoint = (pfn_engine_entrypoint)dlsym(hn, "engine_entrypoint");
	int rv = engine_entrypoint(persist_state, loader_addr);
	dlclose(hn);
	return rv;
}

static void loader_cleanup() {
	dlclose(hn_vulkan_dl);
}

static void registry_listener_global(
	void *data,
	struct wl_registry *registry,
	uint32_t name,
	const char *interface,
	uint32_t version
) {
	window_context_t *ctx = data;
	if (strcmp(interface, wl_compositor_interface.name) == 0) {
		ctx->compositor = wl_registry_bind(registry, name, &wl_compositor_interface, version);
	} else if (strcmp(interface, xdg_wm_base_interface.name) == 0) {
		ctx->xdg_wm_base = wl_registry_bind(registry, name, &xdg_wm_base_interface, version);
	} else if (strcmp(interface, wl_seat_interface.name) == 0) {
		ctx->seat = wl_registry_bind(registry, name, &wl_seat_interface, version);
	} else {
		return;
	}
#ifndef NDEBUG
	printf("%s: binding %s version %d\n", __func__, interface, version);
#endif
}

// noop
static void registry_listener_global_remove(void *, struct wl_registry *, uint32_t) {}

static int loader_window_prepare(window_context_t *ctx) {
	ctx->display = wl_display_connect(NULL);
	ctx->registry = wl_display_get_registry(ctx->display);
	memcpy(&ctx->registry_listener, &(struct wl_registry_listener){
		.global = registry_listener_global,
		.global_remove = registry_listener_global_remove,
	}, sizeof(ctx->registry_listener));
	wl_registry_add_listener(ctx->registry, &ctx->registry_listener, ctx);
	wl_display_flush(ctx->display);
	return 0;
}

static void loader_window_cleanup(window_context_t *ctx) {
	xdg_toplevel_destroy(ctx->xdg_toplevel);
	xdg_surface_destroy(ctx->xdg_surface);
	wl_surface_destroy(ctx->surface);
	xdg_wm_base_destroy(ctx->xdg_wm_base);
	wl_registry_destroy(ctx->registry);
	wl_display_disconnect(ctx->display);
}

static void xdg_wm_base_ping(__attribute__((unused)) void *data, struct xdg_wm_base *xdg_wm_base, uint32_t serial) {
	xdg_wm_base_pong(xdg_wm_base, serial);
}

static void loader_window_xdg_wm_base_pinger(window_context_t *ctx) {
	memcpy(&ctx->xdg_wm_base_listener, &(struct xdg_wm_base_listener){
		.ping = xdg_wm_base_ping,
	}, sizeof(ctx->xdg_wm_base_listener));
	xdg_wm_base_add_listener(ctx->xdg_wm_base, &ctx->xdg_wm_base_listener, ctx);
}

static void xdg_surface_listener_configure(void *data, struct xdg_surface *xdg_surface, uint32_t serial) {
	window_context_t *ctx = data;
	xdg_surface_ack_configure(xdg_surface, serial);
	wl_surface_commit(ctx->surface);
}

static void loader_wayland_surface(window_context_t *ctx) {
	ctx->surface = wl_compositor_create_surface(ctx->compositor);
	ctx->xdg_surface = xdg_wm_base_get_xdg_surface(ctx->xdg_wm_base, ctx->surface);
	memcpy(&ctx->xdg_surface_listener, &(struct xdg_surface_listener){
		.configure = xdg_surface_listener_configure,
	}, sizeof(ctx->xdg_surface_listener));
	xdg_surface_add_listener(ctx->xdg_surface, &ctx->xdg_surface_listener, ctx);

	ctx->xdg_toplevel = xdg_surface_get_toplevel(ctx->xdg_surface);
	xdg_toplevel_set_title(ctx->xdg_toplevel, "application");
}

static void wl_pointer_enter(void *, struct wl_pointer *, uint32_t, struct wl_surface *, wl_fixed_t, wl_fixed_t) {}
static void wl_pointer_leave(void *, struct wl_pointer *, uint32_t, struct wl_surface *) {}
static void wl_pointer_motion(void *data, struct wl_pointer *, uint32_t, wl_fixed_t x, wl_fixed_t y) {
	window_context_t *ctx = data;
	ctx->pointer_position[0] = wl_fixed_to_double(x);
	ctx->pointer_position[1] = wl_fixed_to_double(y);
}
static void wl_pointer_button(void *data, struct wl_pointer *, uint32_t, uint32_t, uint32_t button, uint32_t button_state) {
	window_context_t *ctx = data;
	if (!(ctx->ninputs < sizeof(ctx_window.inputs_buf) / sizeof(*ctx_window.inputs_buf))) { return; }
	input_state_t *ev = &ctx->inputs_buf[ctx->ninputs++];
	switch (button) {
		case BTN_LEFT: button = INPUT_MOUSE1; break;
		case BTN_RIGHT: button = INPUT_MOUSE2; break;
		case BTN_MIDDLE: button = INPUT_MOUSE3; break;
		case BTN_SIDE: button = INPUT_MOUSE4; break;
		case BTN_EXTRA: button = INPUT_MOUSE5; break;
	}
	*ev = (input_state_t){ button_state, button };
}
static void wl_pointer_axis(void *, struct wl_pointer *, uint32_t, uint32_t, wl_fixed_t) {}
static void wl_pointer_frame(void *, struct wl_pointer *) {}
static void wl_pointer_axis_source(void *, struct wl_pointer *, uint32_t) {}
static void wl_pointer_axis_stop(void *, struct wl_pointer *, uint32_t, uint32_t) {}
static void wl_pointer_axis_discrete(void *, struct wl_pointer *, uint32_t, int32_t) {}

static void loader_input_prepare_pointer(window_context_t *ctx) {
	ctx->pointer = wl_seat_get_pointer(ctx->seat);
	ctx->pointer_listener = (struct wl_pointer_listener){
		.enter = wl_pointer_enter,
		.leave = wl_pointer_leave,
		.motion = wl_pointer_motion,
		.button = wl_pointer_button,
		.axis = wl_pointer_axis,
		.frame = wl_pointer_frame,
		.axis_source = wl_pointer_axis_source,
		.axis_stop = wl_pointer_axis_stop,
		.axis_discrete = wl_pointer_axis_discrete,
	};
	wl_pointer_add_listener(ctx->pointer, &ctx->pointer_listener, ctx);
}

static void wl_keyboard_keymap(void *data, struct wl_keyboard *, uint32_t format, int32_t fd, uint32_t size) {
	if (format != WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1) { return; }
	char *shm = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (!shm) { return; }

	window_context_t *ctx = data;
	ctx->keymap = xkb_keymap_new_from_string(
		ctx->xkb_context,
		shm,
		XKB_KEYMAP_FORMAT_TEXT_V1,
		XKB_KEYMAP_COMPILE_NO_FLAGS
	);
	munmap(shm, size);

	ctx->xkb_state = xkb_state_new(ctx->keymap);
	close(fd);
}

static void wl_keyboard_key(void *data, struct wl_keyboard *, uint32_t, uint32_t, uint32_t key, uint32_t state) {
	window_context_t *ctx = data;
	if (!(ctx->ninputs < sizeof(ctx_window.inputs_buf) / sizeof(*ctx_window.inputs_buf))) { return; }
	xkb_keysym_t sym = xkb_state_key_get_one_sym(ctx->xkb_state, key + 8);
	input_state_t *ev = &ctx->inputs_buf[ctx->ninputs++];
	*ev = (input_state_t){ state, sym };
}

static void wl_keyboard_modifiers(void *data, struct wl_keyboard *, uint32_t, uint32_t depressed, uint32_t latched, uint32_t locked, uint32_t group) {
	window_context_t *ctx = data;
	xkb_state_update_mask(ctx->xkb_state, depressed, latched, locked, 0, 0, group);
}
static void wl_keyboard_repeat_info(void *, struct wl_keyboard *, int, int) {}
static void wl_keyboard_enter(void *, struct wl_keyboard *, uint32_t, struct wl_surface *, struct wl_array *) {}
static void wl_keyboard_leave(void *, struct wl_keyboard *, uint32_t, struct wl_surface *) {}

static void loader_input_prepare_keyboard(window_context_t *ctx) {
	ctx->xkb_context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
	ctx->keyboard = wl_seat_get_keyboard(ctx->seat);
	ctx->keyboard_listener = (struct wl_keyboard_listener){
		.keymap = wl_keyboard_keymap,
		.key = wl_keyboard_key,
		.modifiers = wl_keyboard_modifiers,
		.repeat_info = wl_keyboard_repeat_info,
		.enter = wl_keyboard_enter,
		.leave = wl_keyboard_leave,
	};
	wl_keyboard_add_listener(ctx->keyboard, &ctx->keyboard_listener, ctx);
}

int main(__attribute__((unused)) int argc, __attribute__((unused)) char *argv[]) {
	window_context_t *window = &ctx_window;
	if (loader_window_prepare(window)) {
		return 1;
	}
	wl_display_roundtrip(window->display);
	loader_window_xdg_wm_base_pinger(window);
	loader_wayland_surface(window);
	loader_input_prepare_pointer(window);
	loader_input_prepare_keyboard(window);
	persist_state_t *persist_state = &(persist_state_t){ 0 };
	if (loader_prepare(persist_state)) {
		return 1;
	}
	// initialise the exit reason to quit in case the engine load fails
	persist_state->engine_exit_reason = ENGINE_EXIT_QUIT;
	do {
		if (loader_engine_load(persist_state)) {
			// TODO: this list should either be kept up to date, or
			// alternatively propagate the exit code differently
			loader_window_cleanup(window);
			loader_cleanup();
			return 1;
		}
	} while (persist_state->engine_exit_reason == ENGINE_EXIT_RELOAD);
	loader_window_cleanup(window);
	loader_cleanup();
	return 0;
}
