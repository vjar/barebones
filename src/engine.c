#include <stdint.h>
#include "loader.h"
#define VK_NO_PROTOTYPES
#include <vulkan/vulkan.h>         // this would include wayland-client.h ifdef
#include <vulkan/vulkan_wayland.h> // VK_USE_PLATFORM_WAYLAND_KHR. That is not
                                   // needed here, and to avoid that,
                                   // vulkan_wayland.h is instead included
                                   // separately.
#include <assert.h>
#include <cglm/vec2.h>

// global loader pfns
#define PFN_FORMAT(type, name, args, loadername) \
	static pfn_##name name;
PFN_LOADER_LIST
#undef PFN_FORMAT

// global vulkan pfns
#define VULKAN_PFN_LIST \
	VULKAN_PFN_FORMAT(vkCreateWaylandSurfaceKHR) \
	VULKAN_PFN_FORMAT(vkEnumeratePhysicalDevices) \
	VULKAN_PFN_FORMAT(vkGetPhysicalDeviceProperties2) \
	VULKAN_PFN_FORMAT(vkGetPhysicalDeviceFeatures2) \
	VULKAN_PFN_FORMAT(vkGetPhysicalDeviceQueueFamilyProperties2) \
	VULKAN_PFN_FORMAT(vkGetPhysicalDeviceSurfaceSupportKHR) \
	VULKAN_PFN_FORMAT(vkCreateDevice) \
	VULKAN_PFN_FORMAT(vkGetDeviceQueue2) \
	VULKAN_PFN_FORMAT(vkGetPhysicalDeviceSurfaceCapabilitiesKHR) \
	VULKAN_PFN_FORMAT(vkGetPhysicalDeviceSurfaceFormats2KHR) \
	VULKAN_PFN_FORMAT(vkGetPhysicalDeviceSurfacePresentModesKHR) \
	VULKAN_PFN_FORMAT(vkCreateSwapchainKHR) \
	VULKAN_PFN_FORMAT(vkGetSwapchainImagesKHR) \
	VULKAN_PFN_FORMAT(vkCreateImageView) \
	VULKAN_PFN_FORMAT(vkCreateRenderPass2KHR) \
	VULKAN_PFN_FORMAT(vkCreateFramebuffer) \
	VULKAN_PFN_FORMAT(vkCreateCommandPool) \
	VULKAN_PFN_FORMAT(vkAllocateCommandBuffers) \
	VULKAN_PFN_FORMAT(vkCreateSemaphore) \
	VULKAN_PFN_FORMAT(vkCreateFence) \
	VULKAN_PFN_FORMAT(vkWaitForFences) \
	VULKAN_PFN_FORMAT(vkResetFences) \
	VULKAN_PFN_FORMAT(vkAcquireNextImageKHR) \
	VULKAN_PFN_FORMAT(vkResetCommandBuffer) \
	VULKAN_PFN_FORMAT(vkBeginCommandBuffer) \
	VULKAN_PFN_FORMAT(vkEndCommandBuffer) \
	VULKAN_PFN_FORMAT(vkQueueSubmit2) \
	VULKAN_PFN_FORMAT(vkQueuePresentKHR) \
	VULKAN_PFN_FORMAT(vkCmdBeginRenderPass2) \
	VULKAN_PFN_FORMAT(vkCmdEndRenderPass2) \
	VULKAN_PFN_FORMAT(vkCreateShaderModule) \
	VULKAN_PFN_FORMAT(vkCreatePipelineLayout) \
	VULKAN_PFN_FORMAT(vkCreateGraphicsPipelines) \
	VULKAN_PFN_FORMAT(vkCmdBindPipeline) \
	VULKAN_PFN_FORMAT(vkCmdSetViewport) \
	VULKAN_PFN_FORMAT(vkCmdSetScissor) \
	VULKAN_PFN_FORMAT(vkCmdDraw) \
	VULKAN_PFN_FORMAT(vkCreateBuffer) \
	VULKAN_PFN_FORMAT(vkGetBufferMemoryRequirements2KHR) \
	VULKAN_PFN_FORMAT(vkGetPhysicalDeviceMemoryProperties2) \
	VULKAN_PFN_FORMAT(vkAllocateMemory) \
	VULKAN_PFN_FORMAT(vkMapMemory) \
	VULKAN_PFN_FORMAT(vkBindBufferMemory2) \
	VULKAN_PFN_FORMAT(vkCmdBindVertexBuffers2) \
	\
	VULKAN_PFN_FORMAT(vkUnmapMemory) \
	VULKAN_PFN_FORMAT(vkDestroyBuffer) \
	VULKAN_PFN_FORMAT(vkFreeMemory) \
	VULKAN_PFN_FORMAT(vkDestroyPipeline) \
	VULKAN_PFN_FORMAT(vkDestroyPipelineLayout) \
	VULKAN_PFN_FORMAT(vkDestroyShaderModule) \
	VULKAN_PFN_FORMAT(vkDeviceWaitIdle) \
	VULKAN_PFN_FORMAT(vkDestroyFence) \
	VULKAN_PFN_FORMAT(vkDestroySemaphore) \
	VULKAN_PFN_FORMAT(vkFreeCommandBuffers) \
	VULKAN_PFN_FORMAT(vkDestroyCommandPool) \
	VULKAN_PFN_FORMAT(vkDestroyFramebuffer) \
	VULKAN_PFN_FORMAT(vkDestroyRenderPass) \
	VULKAN_PFN_FORMAT(vkDestroyImageView) \
	VULKAN_PFN_FORMAT(vkDestroySwapchainKHR) \
	VULKAN_PFN_FORMAT(vkDestroySurfaceKHR) \
	VULKAN_PFN_FORMAT(vkDestroyDevice) \
	VULKAN_PFN_FORMAT(vkDestroyInstance)

static PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr;
static PFN_vkCreateInstance vkCreateInstance;
#define VULKAN_PFN_FORMAT(name) static PFN_##name name;
VULKAN_PFN_LIST
#undef VULKAN_PFN_FORMAT

#ifdef NDEBUG
#define UNUSED_NDEBUG __attribute__((unused))
#else
#define UNUSED_NDEBUG
#endif

typedef struct mem {
	uint64_t sz; // this starts at zero, and is increased with mem_push
} mem_t;

static void *mem_push(mem_t *mem, uint64_t len) {
	void *addr_at_end = (char*)mem + sizeof(mem_t) + mem->sz;
	mem->sz += len;
	return addr_at_end;
}

static uint64_t mem_address_pos(const mem_t *mem, const void *address) {
	assert((char*)address >= (char*)mem + sizeof(*mem) && "tried to resolve position of address that is outside of mem");
	return (uint64_t)((char*)address - sizeof(*mem) - (char*)mem);
}

UNUSED_NDEBUG static uint64_t mem_grown_since(const mem_t *mem, const void *address) {
	return mem->sz - mem_address_pos(mem, address);
}

#ifdef NDEBUG
#define CONST_IF_NDEBUG const
#else
#define CONST_IF_NDEBUG
#endif
static void mem_return_unchecked(mem_t *mem, CONST_IF_NDEBUG void *address) {
	assert((char*)address >= (char*)mem + sizeof(*mem) && mem_address_pos(mem, address) <= mem->sz && "return address should be inside mem");
	uint64_t new_sz = mem_address_pos(mem, address);
#ifndef NDEBUG
	uint64_t distance_to_end = mem_grown_since(mem, address);
	loader_memset(address, 0, distance_to_end);
#endif
	mem->sz = new_sz;
}

static void mem_return(mem_t *mem, CONST_IF_NDEBUG void *address, UNUSED_NDEBUG uint64_t expected_sz) {
	assert(expected_sz == mem_grown_since(mem, address));
	mem_return_unchecked(mem, address);
}

typedef void (*PFN_gui_rect)(void *data, vec2 pos, vec2 size);
typedef struct gui_callbacks {
	PFN_gui_rect rect;
} gui_callbacks_t;

typedef struct gui_node {
	struct gui_node *first_child;
	struct gui_node *next;
	struct gui_context *ctx;
	vec2 outbounds[2];
	uint32_t z_index;
	void *userdata;
} gui_node_t;

typedef struct gui_context {
	struct {
		void *userdata;
		gui_callbacks_t *callbacks;
		mem_t *mem;
	};
	gui_node_t *nodes;
	gui_node_t *root;
	uint32_t max_z_index;
	vec2 scale;
	vec2 offset;
} gui_context_t;

typedef struct gui_dims {
	// in: given as inputs to the pipeline
	const vec2 in_tl;
	const vec2 in_br;
	// out: the visible output's dimensions, match window size for pixel
	// accuracy
	const vec2 out_tl;
	const vec2 out_br;
} gui_dims_t;

static void gui_set_dims(gui_context_t *ctx, gui_dims_t *dims) {
	ctx->scale[0] = (dims->in_br[0] - dims->in_tl[0]) / (dims->out_br[0] - dims->out_tl[0]);
	ctx->scale[1] = (dims->in_br[1] - dims->in_tl[1]) / (dims->out_br[1] - dims->out_tl[1]);
	ctx->offset[0] = dims->in_tl[0];
	ctx->offset[1] = dims->in_tl[1];
}

static gui_node_t **gui_new_child(gui_node_t *parent) {
	gui_node_t *n = parent->first_child;
	if (n == NULL) { return &parent->first_child; }
	for (;;) {
		if (n->next == NULL) {
			return &n->next;
		}

		n = n->next;
	}
	assert("this should be unreachable");
	return &parent->first_child;
}

__attribute__((unused)) static uint32_t gui_count_children(gui_node_t *parent) {
	uint32_t nchildren = 0;
	for (gui_node_t *n = parent->first_child; n != NULL; n = n->next) {
		nchildren++;
	}
	return nchildren;
}

static void gui_titled_panel(gui_node_t *parent, vec2 outpos, vec2 outsize, void *userdata) {
	gui_context_t *ctx = parent->ctx;
	gui_node_t *node = mem_push(ctx->mem, sizeof(*node));
	node->z_index = ctx->max_z_index++;
	node->userdata = userdata;
	gui_node_t **new_child = gui_new_child(parent);
	*new_child = node;
	gui_callbacks_t *cb = ctx->callbacks;
	vec2 inpos, insize;

	glm_vec2_mul(outpos, ctx->scale, inpos);
	glm_vec2_mul(outsize, ctx->scale, insize);
	glm_vec2_add(inpos, ctx->offset, inpos);
	cb->rect(ctx->userdata, inpos, (vec2){ insize[0], 0.04f });
	glm_vec2_add(inpos, (vec2){ 0.0f, 0.06f }, inpos);
	cb->rect(ctx->userdata, inpos, insize);

	glm_vec2_copy(outpos, node->outbounds[0]);
	glm_vec2_add(outpos, outsize, node->outbounds[1]);
}
/* gui end */

typedef struct timestamp {
	uint32_t sec, nsec;
} timestamp_t;

enum event_type {
	EVENT_TEST = 1,
	EVENT_CVAR_SET,
	EVENT_RI_MESH,
	EVENT_RI_MATERIAL,
};

typedef struct event_test {
	enum event_type type;
	timestamp_t t;
	const char *cstr;
} event_test_t;

enum cvar_variant {
	CVAR_TEST = 1,
	CVAR_R_VSYNC,
	CVAR_R_WIDTH,
	CVAR_R_HEIGHT,
	CVAR_R_SWAPCHAIN_NIMAGES,
	CVAR_R_NCONCURRENT_FRAMES,
	CVAR_R_VERT,
	CVAR_R_FRAG,
	CVAR_R_CHART_VERT,
	CVAR_R_CHART_FRAG,
};

typedef struct event_cvar_set {
	enum event_type type;
	timestamp_t t;
	enum cvar_variant variant;
	const char *cstr;
	uint64_t numeric;
} event_cvar_set_t;

typedef struct event_ri_mesh {
	enum event_type type;
	timestamp_t t;
	vec2 *vertices;
	uint64_t nvertices;
} event_ri_mesh_t;

typedef struct event_ri_material {
	enum event_type type;
	timestamp_t t;
	VkPipeline pipeline;
} event_ri_material_t;

typedef union event {
	struct {
		enum event_type type;
		timestamp_t t;
	};
	event_test_t test;
	event_cvar_set_t cvar_set;
	event_ri_mesh_t ri_mesh;
	event_ri_material_t ri_material;
} event_t;

typedef struct logger {
	uint64_t nevents;
	timestamp_t reference;
	const uint64_t prefix_len;
	char *prefix;
	const uint64_t eventssz;
	event_t *events;
} logger_t;

static logger_t *logger_create(mem_t *mem, timestamp_t reference, const char *logger_prefix) {
	const uint64_t MiB = 1 << 20;
	const uint64_t eventssz = 1 * MiB;
	logger_t *logger = mem_push(mem, sizeof(*logger));
	const uint64_t prefix_len = loader_strlen(logger_prefix);
	loader_memcpy(logger, &(logger_t){
		.nevents = 0,
		.reference = reference,
		.prefix_len = prefix_len,
		.prefix = mem_push(mem, prefix_len),
		.eventssz = eventssz,
		.events = mem_push(mem, eventssz),
	}, sizeof(*logger));
	loader_memcpy(logger->prefix, logger_prefix, prefix_len);
	return logger;
}

static void logger_destroy(mem_t *mem, logger_t *logger) {
	mem_return(mem, logger->events, logger->eventssz);
	mem_return(mem, logger->prefix, logger->prefix_len);
	mem_return(mem, logger, sizeof(*logger));
}

#ifndef NDEBUG
#define DBG(...) logev(__VA_ARGS__)
#else
#define DBG(...)
#endif

static int logev(logger_t *logger, event_t *event) {
	/* reference_sec is subtracted from every logged message for increased
	   accuracy later, when casting to floats
	 * reference_nsec is subtracted from the float later, when formatting
	   the message
	 * we may end up with negative values here if the system time (in UTC)
	   jumps backwards
	*/
	loader_timestamp(&event->t.sec, &event->t.nsec); // sec is immediately overwritten
	event->t.sec -= logger->reference.sec; // store seconds starting from 0
	loader_memcpy(&logger->events[logger->nevents++], event, sizeof(*event));
	return 0;
}

static void log_print_event(event_t *ev, float reference_nsec, const char *prefix) {
	float ev_time = (float)ev->t.sec + ev->t.nsec * 1e-9f - reference_nsec;
	switch (ev->type) {
		case EVENT_TEST:
			loader_printf("[%s] TEST ms=%.3f cstr=\"%s\"\n", prefix, ev_time * 1e3f, ((event_test_t *)ev)->cstr);
			break;
		case EVENT_CVAR_SET:
			loader_printf("[%s] CVAR_SET ms=%.3f variant=%d numeric=%d cstr=\"%s\"\n",
					prefix,
					ev_time * 1e3f,
					((event_cvar_set_t *)ev)->variant,
					((event_cvar_set_t *)ev)->numeric,
					((event_cvar_set_t *)ev)->cstr
			);
			break;
		default:
			break;
	}
}

static void log_print(logger_t *logger, uint64_t start_pos, uint64_t *save_pos) {
	const uint64_t nevents = logger->nevents;
	const float reference_nsec = logger->reference.nsec * 1e-9f;
	loader_printf("[%s] begin logger dump (skipping %d previously consumed events)\n", logger->prefix, start_pos);
	for (uint64_t i = start_pos; i < nevents; i++) {
		event_t *ev = &logger->events[i];
		log_print_event(ev, reference_nsec, logger->prefix);
	}
	if (save_pos) {
		*save_pos = nevents;
	}
}

typedef struct log_consumer {
	logger_t *logger;
	uint64_t pos;
} log_consumer_t;

static log_consumer_t *log_consumer_create(mem_t *mem, logger_t *logger) {
	log_consumer_t *consumer = mem_push(mem, sizeof(*consumer));
	loader_memcpy(consumer, &(log_consumer_t){
		.logger = logger,
		.pos = 0,
	}, sizeof(*consumer));
	return consumer;
}

static void log_consumer_destroy(mem_t *mem, log_consumer_t *consumer) {
	mem_return(mem, consumer, sizeof(*consumer));
}

typedef struct engine_state {
	uint64_t nentries;
	timestamp_t reference_time;
	uint64_t memsz;
	mem_t *mem;
	logger_t *engine_logger;
	struct renderer_context *r_ctx;
	struct cvar_context *cvar_ctx;
	log_consumer_t *consumer_r;
	log_consumer_t *consumer_cvar;
	struct ri_context *ri_ctx;
	gui_context_t *gui_ctx;
} engine_state_t;

typedef struct cvar_context {
	engine_state_t *engine_state;
	logger_t *logger;
} cvar_context_t;

static void cvar_create_default_renderer_events(cvar_context_t *ctx) {
	logev(ctx->logger, &(event_t){.cvar_set = {EVENT_CVAR_SET, .variant = CVAR_R_VSYNC, .numeric = 1}});
	logev(ctx->logger, &(event_t){.cvar_set = {EVENT_CVAR_SET, .variant = CVAR_R_WIDTH, .numeric = 800}});
	logev(ctx->logger, &(event_t){.cvar_set = {EVENT_CVAR_SET, .variant = CVAR_R_HEIGHT, .numeric = 600}});
	/* Send nimages cvar_set events from 1 to n. Values outside
	   surface_capabilities will be ignored */
	logev(ctx->logger, &(event_t){.cvar_set = {EVENT_CVAR_SET, .variant = CVAR_R_SWAPCHAIN_NIMAGES, .numeric = 1}});
	logev(ctx->logger, &(event_t){.cvar_set = {EVENT_CVAR_SET, .variant = CVAR_R_SWAPCHAIN_NIMAGES, .numeric = 2}});
	logev(ctx->logger, &(event_t){.cvar_set = {EVENT_CVAR_SET, .variant = CVAR_R_NCONCURRENT_FRAMES, .numeric = 2}});
	logev(ctx->logger, &(event_t){.cvar_set = {EVENT_CVAR_SET, .variant = CVAR_R_VERT, .cstr = "shader.vert.spirv"}});
	logev(ctx->logger, &(event_t){.cvar_set = {EVENT_CVAR_SET, .variant = CVAR_R_FRAG, .cstr = "white.frag.spirv"}});
	logev(ctx->logger, &(event_t){.cvar_set = {EVENT_CVAR_SET, .variant = CVAR_R_CHART_VERT, .cstr = "shader.vert.spirv"}});
	logev(ctx->logger, &(event_t){.cvar_set = {EVENT_CVAR_SET, .variant = CVAR_R_CHART_FRAG, .cstr = "graph_area.frag.spirv"}});
}

static cvar_context_t *cvar_prepare(engine_state_t *engine_state) {
	cvar_context_t *ctx = mem_push(engine_state->mem, sizeof(*ctx));
	loader_memcpy(ctx, &(cvar_context_t){
		.engine_state = engine_state,
		.logger = logger_create(engine_state->mem, engine_state->reference_time, "cvar"),
	}, sizeof(*ctx));
	return ctx;
}

static void cvar_cleanup(mem_t *mem, cvar_context_t *ctx) {
	logger_destroy(mem, ctx->logger);
	mem_return(mem, ctx, sizeof(*ctx));
}

typedef struct renderer_context {
	engine_state_t *engine_state;
	logger_t *logger;
	log_consumer_t *consumer_cvar;
	uint64_t stages_initialised;
	struct {
		VkInstance instance;
		VkPhysicalDevice physical_device;
		uint32_t qf_graphics;
		uint32_t qf_present;
		VkSurfaceKHR surface;
		VkDevice device;
		VkQueue q_graphics;
		VkQueue q_present;
		VkSurfaceCapabilitiesKHR surface_capabilities;
	};
	struct {
		VkSwapchainKHR swapchain;
		uint32_t swapchain_nimages;
		VkFormat swapchain_format;
		VkExtent2D swapchain_extent;
		VkImage *im_swapchain;
		VkImageView *imview_swapchain;
	};
	struct {
		VkFramebuffer *framebuffers;
		VkRenderPass renderpass;
	};
	struct {
		uint32_t frame_num;
		VkCommandPool commandpool;
		VkCommandBuffer *commandbuffers;
		uint32_t nconcurrent_frames;
	};
	struct {
		VkSemaphore *sems_image_available;
		VkSemaphore *sems_render_finished;
		VkFence *fens_inflight;
	};
	VkPipelineLayout pipeline_layout;
	struct renderer_pipeline {
		VkShaderModule shader_vert;
		VkShaderModule shader_frag;
		VkPipeline graphics_pipeline;
	} pipeline, pipeline_chart;
	struct {
		VkDeviceMemory mem_vbuf;
		VkBuffer vbuf;
		VkDeviceSize mem_vbufsz;
	};
	log_consumer_t *consumer_ri;
} renderer_context_t;

enum r_conf_type {
	R_CONF_SWAPCHAIN = 1,
	R_CONF_PIPELINE,
	R_CONF_PIPELINE_CHART,
	R_CONF_MAX,
};

typedef struct r_conf_swapchain {
	_Bool vsync;
	uint32_t nconcurrent_frames;
	uint32_t nimages;
	uint32_t width, height;
	VkSurfaceTransformFlagBitsKHR surface_transform;
} r_conf_swapchain_t;

typedef struct r_conf_pipeline {
	VkShaderModule shader_vert;
	VkShaderModule shader_frag;
} r_conf_pipeline_t;

typedef struct ri_context {
	engine_state_t *engine_state;
	logger_t *logger;
} ri_context_t;

static ri_context_t *ri_prepare(engine_state_t *engine_state) {
	ri_context_t *ctx = mem_push(engine_state->mem, sizeof(*ctx));
	loader_memcpy(ctx, &(ri_context_t){
		.engine_state = engine_state,
		.logger = logger_create(engine_state->mem, engine_state->reference_time, "ri"),
	}, sizeof(*ctx));
	return ctx;
}

static void ri_cleanup(mem_t *mem, ri_context_t *ctx) {
	logger_destroy(mem, ctx->logger);
	mem_return(mem, ctx, sizeof(*ctx));
}

static void r_physicaldevice(renderer_context_t *ctx) {
	__label__ done_success;
	CONST_IF_NDEBUG void *mem_checkpoint = mem_push(ctx->engine_state->mem, 0);

	const uint32_t pdsz = 16;
	VkPhysicalDevice *pd;
	const uint32_t nbytes = pdsz * sizeof(*pd);
	pd = mem_push(ctx->engine_state->mem, nbytes);

	uint32_t npd = pdsz;
	vkEnumeratePhysicalDevices(ctx->instance, &npd, pd);

	VkPhysicalDeviceProperties2 props = {
		// NOTE: the API mishandles this struct if sType is wrong
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2,
	};
	VkPhysicalDeviceFeatures2 feats = {
		// NOTE: the API mishandles this struct if sType is wrong
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2,
	};
	ctx->physical_device = NULL;
	for (uint32_t i = 0; i < npd; i++) {
		vkGetPhysicalDeviceProperties2(pd[i], &props);
		vkGetPhysicalDeviceFeatures2(pd[i], &feats);

		if (!feats.features.geometryShader) { continue; }
		ctx->physical_device = pd[i];
		goto done_success;
	}

	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "none of the available vkphysicaldevices are suitable"}});
done_success:
	mem_return(ctx->engine_state->mem, mem_checkpoint, nbytes);
}

static int r_queuefamilies(renderer_context_t *ctx) {
	__label__ done_success;
	int rc = 0;
	CONST_IF_NDEBUG void *mem_checkpoint = mem_push(ctx->engine_state->mem, 0);

	const uint32_t psz = 16; // props (=properties) size
	VkQueueFamilyProperties2 *p;
	const uint32_t nbytes = psz * sizeof(*p);
	p = mem_push(ctx->engine_state->mem, nbytes);
	for (uint32_t i = 0; i < psz; i++) {
		p[i] = (VkQueueFamilyProperties2){
			.sType = VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2
		};
	};

	uint32_t np = psz; // nprops
	vkGetPhysicalDeviceQueueFamilyProperties2(ctx->physical_device, &np, p);

	// the loop will never assign np, so use that to check if suitable
	// queuefamilies were found
	ctx->qf_graphics = ctx->qf_present = np;
	for (uint32_t i = 0; i < np; i++) {
		if (p[i].queueFamilyProperties.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			ctx->qf_graphics = i;
			DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "suitable qf_graphics found"}});
			// exit now if the other one is done
			if (ctx->qf_present != np) { goto done_success; }
		}

		VkBool32 present_supported;
		vkGetPhysicalDeviceSurfaceSupportKHR(
			ctx->physical_device,
			i,
			ctx->surface,
			&present_supported
		);
		if (present_supported == VK_TRUE) {
			ctx->qf_present = i;
			DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "suitable qf_present found"}});
			if (ctx->qf_present != np) { goto done_success; }
		}
	}

	// all choices are exhausted, exit with error
	rc = 1;
	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "available queuefamilies are not suitable"}});
done_success:
	mem_return(ctx->engine_state->mem, mem_checkpoint, nbytes);
	return rc;
}

#define VULKAN_PFN_FORMAT(name) \
	name = (PFN_##name)vkGetInstanceProcAddr(ctx->instance, #name); \
	if (!name) { return 1; }
static int r_vulkan_proc_addrs_resolve(renderer_context_t *ctx) {
	VULKAN_PFN_LIST
	return 0;
}
#undef VULKAN_PFN_FORMAT

static int r_prepare_framebuffers(renderer_context_t *ctx) {
	ctx->framebuffers = mem_push(ctx->engine_state->mem, ctx->swapchain_nimages * sizeof(*ctx->framebuffers));
	for (uint32_t i = 0; i < ctx->swapchain_nimages; i++) {
		if (vkCreateFramebuffer(ctx->device, &(VkFramebufferCreateInfo){
			.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
			.renderPass = ctx->renderpass,
			.pAttachments = &(VkImageView){ ctx->imview_swapchain[i] },
			.attachmentCount = 1,
			.width = ctx->swapchain_extent.width,
			.height = ctx->swapchain_extent.height,
			.layers = 1,
		}, NULL, &ctx->framebuffers[i]) != VK_SUCCESS) {
			return 1;
		}
	}
	return 0;
}

static int r_vulkan_proc_addrs_prepare() {
	vkGetInstanceProcAddr = (PFN_vkGetInstanceProcAddr)loader_vulkan_sym("vkGetInstanceProcAddr");
	vkCreateInstance = (PFN_vkCreateInstance)vkGetInstanceProcAddr(NULL, "vkCreateInstance");
	if (!vkCreateInstance) { return 1; }
	return 0;
}

static int r_prepare_base(renderer_context_t *ctx) {
	if (r_vulkan_proc_addrs_prepare()) { return 1; }
	VkApplicationInfo application = {
		.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
		.pApplicationName = "application",
		.pEngineName = "engine",
		.applicationVersion = VK_MAKE_VERSION(0, 1, 0),
		.engineVersion = VK_MAKE_VERSION(0, 1, 0),
		.apiVersion = VK_API_VERSION_1_3,
	};
	const char *enabled_extension_names[] = {
		VK_KHR_SURFACE_EXTENSION_NAME,
		VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME,
		VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME,
		VK_KHR_GET_SURFACE_CAPABILITIES_2_EXTENSION_NAME,
	};
	const char *enabled_layer_names[] = {
		"VK_LAYER_KHRONOS_validation",
	};
	VkInstanceCreateInfo instance_create = {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.pApplicationInfo = &application,
		.ppEnabledExtensionNames = enabled_extension_names,
		.enabledExtensionCount = sizeof(enabled_extension_names) / sizeof(*enabled_extension_names),
		.ppEnabledLayerNames = enabled_layer_names,
		.enabledLayerCount = sizeof(enabled_layer_names) / sizeof(*enabled_layer_names),
	};
	VkResult rv = vkCreateInstance(&instance_create, NULL, &ctx->instance);
	if (rv != VK_SUCCESS) { return 1; }
	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST,.cstr = "vk instance created"}});

	// reloading all procs here isn't a big deal, since the kind-of-cached
	// state is seldom invalidated by a cvar being set
	if (r_vulkan_proc_addrs_resolve(ctx)) { return 1; };
	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "vk pfns resolved"}});

	vkCreateWaylandSurfaceKHR(ctx->instance, &(VkWaylandSurfaceCreateInfoKHR){
		.sType = VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR,
		.display = loader_wayland_display_get(),
		.surface = loader_wayland_surface_get(),
	}, NULL, &ctx->surface);
	if (!ctx->surface) { return 1; }
	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "vk wayland surface created"}});

	r_physicaldevice(ctx);
	if (!ctx->physical_device) { return 1; }
	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "vk physical device ok"}});

	if (r_queuefamilies(ctx)) { return 1; }
	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "vk queue families ok"}});

	float queue_priorities[] = { 1.0f, 1.0f };
	vkCreateDevice(ctx->physical_device, &(VkDeviceCreateInfo){
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.queueCreateInfoCount = 1,
		.pQueueCreateInfos = (VkDeviceQueueCreateInfo[]){
			{
				.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
				.queueFamilyIndex = ctx->qf_present,
				.queueCount = 1,
				.pQueuePriorities = &queue_priorities[0],
			},
			{
				.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
				.queueFamilyIndex = ctx->qf_graphics,
				.queueCount = 1,
				.pQueuePriorities = &queue_priorities[1],
			},
		},
		.enabledExtensionCount = 3,
		.ppEnabledExtensionNames = (const char*[]){
			VK_KHR_SWAPCHAIN_EXTENSION_NAME,
			VK_KHR_CREATE_RENDERPASS_2_EXTENSION_NAME,
			VK_KHR_GET_MEMORY_REQUIREMENTS_2_EXTENSION_NAME,
		},
		.pNext = &(VkPhysicalDeviceFeatures2){
			.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2,
			.features = {
				.samplerAnisotropy = VK_TRUE,
				.dualSrcBlend = VK_TRUE,
			},
			.pNext = &(VkPhysicalDeviceVulkan13Features){
				.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_FEATURES,
				.synchronization2 = VK_TRUE,
			},
		},
	}, NULL, &ctx->device);
	if (!ctx->device) { return 1; }
	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "vk device ok"}});

	vkGetDeviceQueue2(ctx->device, &(VkDeviceQueueInfo2){
		.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_INFO_2,
		.queueFamilyIndex = ctx->qf_graphics,
		.queueIndex = 0,
	}, &ctx->q_graphics);
	vkGetDeviceQueue2(ctx->device, &(VkDeviceQueueInfo2){
		.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_INFO_2,
		.queueFamilyIndex = ctx->qf_present,
		.queueIndex = 0,
	}, &ctx->q_present);

	if (vkCreateBuffer(ctx->device, &(VkBufferCreateInfo){
		.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
		.size = sizeof(vec2) * 2000000, // TODO 2M vertices may be a bit excessive
		.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
		.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
	}, NULL, &ctx->vbuf) != VK_SUCCESS) { return 1; }

	VkMemoryRequirements2 memrq = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2_KHR,
	};
	vkGetBufferMemoryRequirements2KHR(ctx->device, &(VkBufferMemoryRequirementsInfo2){
		.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_REQUIREMENTS_INFO_2,
		.buffer = ctx->vbuf,
	}, &memrq);
	if (!memrq.memoryRequirements.size) { return 1; }

	VkPhysicalDeviceMemoryProperties2 props = {
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2,
	};
	vkGetPhysicalDeviceMemoryProperties2(ctx->physical_device, &props);

	uint32_t filter_type = memrq.memoryRequirements.memoryTypeBits;
	VkMemoryPropertyFlags filter_props = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
	uint32_t memory_type_index = props.memoryProperties.memoryTypeCount;
	for (uint32_t type = 0; type < props.memoryProperties.memoryTypeCount; type++) {
		if (!(filter_type & (1 << type))) { continue; }
		const VkMemoryPropertyFlags flags = props.memoryProperties.memoryTypes[type].propertyFlags;
		if (!(flags & filter_props)) { continue; }
		// return type;
		memory_type_index = type;
		break;
	}
	if (memory_type_index == props.memoryProperties.memoryTypeCount) { return 1; }

	if (vkAllocateMemory(ctx->device, &(VkMemoryAllocateInfo){
		.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		.allocationSize = memrq.memoryRequirements.size,
		.memoryTypeIndex = memory_type_index,
	}, NULL, &ctx->mem_vbuf) != VK_SUCCESS) { return 1; }
	ctx->mem_vbufsz = memrq.memoryRequirements.size;

	if (vkBindBufferMemory2(ctx->device, 1, &(VkBindBufferMemoryInfo){
		.sType = VK_STRUCTURE_TYPE_BIND_BUFFER_MEMORY_INFO,
		.buffer = ctx->vbuf,
		.memory = ctx->mem_vbuf,
		.memoryOffset = 0,
	}) != VK_SUCCESS) { return 1; }

	if (vkGetPhysicalDeviceSurfaceCapabilitiesKHR(ctx->physical_device, ctx->surface, &ctx->surface_capabilities) != VK_SUCCESS) { return 1; }

	if (vkCreatePipelineLayout(ctx->device, &(VkPipelineLayoutCreateInfo){
		.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
	}, NULL, &ctx->pipeline_layout) != VK_SUCCESS) { return 1; }
	return 0;
}

static int r_swapchain_surface_format(renderer_context_t *ctx, VkSurfaceFormatKHR *dst) {
	const uint32_t fsz = 16;
	VkSurfaceFormat2KHR f[fsz];
	for (uint32_t i = 0; i < fsz; i++) {
		f[i] = (VkSurfaceFormat2KHR){
			.sType = VK_STRUCTURE_TYPE_SURFACE_FORMAT_2_KHR,
		};
	}

	uint32_t nf = fsz;
	if (vkGetPhysicalDeviceSurfaceFormats2KHR(ctx->physical_device, &(VkPhysicalDeviceSurfaceInfo2KHR){
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SURFACE_INFO_2_KHR,
		.surface = ctx->surface,
	}, &nf, f) != VK_SUCCESS) {
		return 1;
	}

	// see if any matches our preference
	for (uint32_t i = 0; i < nf; i++) {
		const VkFormat format = f[i].surfaceFormat.format;
		const VkColorSpaceKHR colorspace = f[i].surfaceFormat.colorSpace;
		if (format == VK_FORMAT_B8G8R8A8_SRGB && colorspace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			*dst = f[i].surfaceFormat;
			return 0;
		}
	}

	// otherwise continue with the first one
	*dst = f[0].surfaceFormat;
	return 0;
}

static int r_swapchain_present_mode(renderer_context_t *ctx, VkPresentModeKHR preference, VkPresentModeKHR *dst) {
	const uint32_t msz = 16;
	VkPresentModeKHR m[msz];

	uint32_t nm = msz;
	if (vkGetPhysicalDeviceSurfacePresentModesKHR(ctx->physical_device, ctx->surface, &nm, m) != VK_SUCCESS) {
		return 1;
	}

	// see if any matches our preference
	for (uint32_t i = 0; i < nm; i++) {
		if (m[i] == preference) {
			*dst = m[i];
			return 0;
		}
	}

	// otherwise continue with the first one
	*dst = m[0];
	return 0;
}

static int r_prepare_swapchain_imageviews(renderer_context_t *ctx) {
	ctx->imview_swapchain = mem_push(ctx->engine_state->mem, ctx->swapchain_nimages * sizeof(*ctx->imview_swapchain));

	for (uint32_t i = 0; i < ctx->swapchain_nimages; i++) {
		if (vkCreateImageView(ctx->device, &(VkImageViewCreateInfo){
			.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
			.image = ctx->im_swapchain[i],
			.viewType = VK_IMAGE_VIEW_TYPE_2D,
			.format = ctx->swapchain_format,
			.components = { VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY },
			.subresourceRange = {
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.levelCount = 1,
				.layerCount = 1,
			},
		}, NULL, &ctx->imview_swapchain[i]) != VK_SUCCESS) { return 1; }
	}
	return 0;
}

static int r_prepare_swapchain(renderer_context_t *ctx, r_conf_swapchain_t *conf) {
	// r_prepare_swapchain_imageviews shortcut
	// r_prepare_framebuffers shortcut
	VkSurfaceFormatKHR format;
	if (r_swapchain_surface_format(ctx, &format)) { return 1; }

	VkPresentModeKHR present_mode;
	uint32_t present_mode_preference = conf->vsync ? VK_PRESENT_MODE_FIFO_KHR : VK_PRESENT_MODE_MAILBOX_KHR;
	if (r_swapchain_present_mode(ctx, present_mode_preference, &present_mode)) { return 1; }

	VkExtent2D extent = {
		.width = conf->width,
		.height = conf->height,
	};

	ctx->nconcurrent_frames = conf->nconcurrent_frames;
	VkSharingMode image_sharing_mode = VK_SHARING_MODE_CONCURRENT;
	uint32_t queue_family_index_count = 2;
	uint32_t *queue_family_indices = (uint32_t[]){ ctx->qf_graphics, ctx->qf_present };
	if (ctx->qf_graphics == ctx->qf_present) {
		image_sharing_mode = VK_SHARING_MODE_EXCLUSIVE;
		queue_family_index_count = 0;
		queue_family_indices = NULL;
	}

	ctx->swapchain_nimages = conf->nimages;
	ctx->swapchain_format = format.format;
	ctx->swapchain_extent = extent;
	if (vkCreateSwapchainKHR(ctx->device, &(VkSwapchainCreateInfoKHR){
		.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		.surface = ctx->surface,
		.minImageCount = ctx->swapchain_nimages,
		.imageFormat = ctx->swapchain_format,
		.imageColorSpace = format.colorSpace,
		.imageExtent = ctx->swapchain_extent,
		.imageArrayLayers = 1,
		.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		.imageSharingMode = image_sharing_mode,
		.queueFamilyIndexCount = queue_family_index_count,
		.pQueueFamilyIndices = queue_family_indices,
		.preTransform = conf->surface_transform,
		.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
		.presentMode = present_mode,
		.clipped = VK_TRUE,
	}, NULL, &ctx->swapchain) != VK_SUCCESS) { return 1; }
	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "vk swapchain created"}});

	ctx->im_swapchain = mem_push(ctx->engine_state->mem, ctx->swapchain_nimages * sizeof(*ctx->im_swapchain));
	uint32_t nimages = ctx->swapchain_nimages;
	if (vkGetSwapchainImagesKHR(ctx->device, ctx->swapchain, &nimages, ctx->im_swapchain) != VK_SUCCESS) { return 1; }
	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "vk swapchain images ok"}});

	r_prepare_swapchain_imageviews(ctx);

	if (vkCreateRenderPass2KHR(ctx->device, &(VkRenderPassCreateInfo2){
		.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2,
		.attachmentCount = 1,
		.pAttachments = (VkAttachmentDescription2[]){
			{
				.sType = VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2,
				.format = ctx->swapchain_format,
				.samples = VK_SAMPLE_COUNT_1_BIT,
				.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
				.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
				.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
				.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
				.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
				.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
			},
		},
		.subpassCount = 1,
		.pSubpasses = &(VkSubpassDescription2){
			.sType = VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_2,
			.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
			.colorAttachmentCount = 1,
			.pColorAttachments = &(VkAttachmentReference2){
				.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2,
				.attachment = 0,
				.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
			},
			//.pDepthStencilAttachment = &(VkAttachmentReference2){},
		},
		.dependencyCount = 1,
		.pDependencies = (VkSubpassDependency2[]){
			{
				.sType = VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2,
				.srcSubpass = VK_SUBPASS_EXTERNAL,
				.dstSubpass = 0,
				.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
				.srcAccessMask = 0,
				.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
				.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
			}
		},
	}, NULL, &ctx->renderpass)) { return 1; }
	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "vk render pass created"}});

	if (r_prepare_framebuffers(ctx)) { return 1; }
	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "vk framebuffers created"}});

	if (vkCreateCommandPool(ctx->device, &(VkCommandPoolCreateInfo){
		.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
		.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
		.queueFamilyIndex = ctx->qf_graphics,
	}, NULL, &ctx->commandpool) != VK_SUCCESS) { return 1; }
	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "vk commandpool created"}});

	ctx->commandbuffers = mem_push(ctx->engine_state->mem, conf->nconcurrent_frames * sizeof(*ctx->commandbuffers));
	if (vkAllocateCommandBuffers(ctx->device, &(VkCommandBufferAllocateInfo){
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		.commandPool = ctx->commandpool,
		.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
		.commandBufferCount = conf->nconcurrent_frames,
	}, ctx->commandbuffers) != VK_SUCCESS) { return 1; }
	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "vk commandbuffers allocated"}});

	ctx->sems_image_available = mem_push(ctx->engine_state->mem, conf->nconcurrent_frames * sizeof(*ctx->sems_image_available));
	ctx->sems_render_finished = mem_push(ctx->engine_state->mem, conf->nconcurrent_frames * sizeof(*ctx->sems_render_finished));
	ctx->fens_inflight = mem_push(ctx->engine_state->mem, conf->nconcurrent_frames * sizeof(*ctx->fens_inflight));
	for (uint32_t i = 0; i < conf->nconcurrent_frames; i++) {
		if (vkCreateSemaphore(ctx->device, &(VkSemaphoreCreateInfo){
			.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
		}, NULL, &ctx->sems_image_available[i]) != VK_SUCCESS) { return 1; }
		if (vkCreateSemaphore(ctx->device, &(VkSemaphoreCreateInfo){
			.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
		}, NULL, &ctx->sems_render_finished[i]) != VK_SUCCESS) { return 1; }
		if (vkCreateFence(ctx->device, &(VkFenceCreateInfo){
			.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
			.flags = VK_FENCE_CREATE_SIGNALED_BIT,
		}, NULL, &ctx->fens_inflight[i]) != VK_SUCCESS) { return 1; }
	}
	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "vk sync objects created"}});
	return 0;
}

static int r_prepare_pipeline_shader(renderer_context_t *ctx, const char *filename, VkShaderModule *shadermodule) {
	uint64_t filesz = 0;
	if (loader_file_read(NULL, &filesz, filename)) { return 1; }
	char *spirv = mem_push(ctx->engine_state->mem, filesz);
	if (loader_file_read(spirv, &filesz, filename)) { return 1; }
	if (vkCreateShaderModule(ctx->device, &(VkShaderModuleCreateInfo){
		.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
		.codeSize = filesz,
		.pCode = (uint32_t *)spirv,
	}, NULL, shadermodule) != VK_SUCCESS) { return 1; }
	mem_return(ctx->engine_state->mem, spirv, filesz);
	return 0;
}

static int r_prepare_pipeline(renderer_context_t *ctx, struct renderer_pipeline *pipeline, r_conf_pipeline_t *conf) {
	pipeline->shader_vert = conf->shader_vert;
	pipeline->shader_frag = conf->shader_frag;
	if (vkCreateGraphicsPipelines(ctx->device, VK_NULL_HANDLE, 1, &(VkGraphicsPipelineCreateInfo){
		.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
		.stageCount = 2,
		.pStages = (VkPipelineShaderStageCreateInfo[]){
			{
				.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				.stage = VK_SHADER_STAGE_VERTEX_BIT,
				.module = pipeline->shader_vert,
				.pName = "main",
			},
			{
				.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
				.stage = VK_SHADER_STAGE_FRAGMENT_BIT,
				.module = pipeline->shader_frag,
				.pName = "main",
			},
		},
		.pVertexInputState = &(VkPipelineVertexInputStateCreateInfo){
			.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
			.vertexBindingDescriptionCount = 1,
			.pVertexBindingDescriptions = &(VkVertexInputBindingDescription){
				.binding = 0,
				.stride = sizeof(vec2),
				.inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
			},
			.vertexAttributeDescriptionCount = 1,
			.pVertexAttributeDescriptions = &(VkVertexInputAttributeDescription){
				.location = 0,
				.binding = 0,
				.format = VK_FORMAT_R32G32_SFLOAT,
				.offset = 0,
			},
		},
		.pInputAssemblyState = &(VkPipelineInputAssemblyStateCreateInfo){
			.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
			.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
			.primitiveRestartEnable = VK_FALSE,
		},
		.pViewportState = &(VkPipelineViewportStateCreateInfo){
			.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
			.viewportCount = 1,
			.pViewports = &(VkViewport){
				.x = 0.0f,
				.y = 0.0f,
				.width = ctx->swapchain_extent.width,
				.height = ctx->swapchain_extent.height,
				.minDepth = 0.0f,
				.maxDepth = 1.0f,
			},
			.scissorCount = 1,
			.pScissors = &(VkRect2D){
				.offset = { 0, 0 },
				.extent = ctx->swapchain_extent,
			},
		},
		.pRasterizationState = &(VkPipelineRasterizationStateCreateInfo){
			.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
			.depthClampEnable = VK_FALSE,
			.rasterizerDiscardEnable = VK_FALSE,
			.polygonMode = VK_POLYGON_MODE_FILL,
			.lineWidth = 1.0f,
			.cullMode = VK_CULL_MODE_BACK_BIT,
			.frontFace = VK_FRONT_FACE_CLOCKWISE,
			.depthBiasEnable = VK_FALSE,
		},
		.pMultisampleState = &(VkPipelineMultisampleStateCreateInfo){
			.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
			.sampleShadingEnable = VK_FALSE,
			.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
		},
		.pDepthStencilState = NULL,
		.pColorBlendState = &(VkPipelineColorBlendStateCreateInfo){
			.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
			.logicOpEnable = VK_FALSE,
			.attachmentCount = 1,
			.pAttachments = &(VkPipelineColorBlendAttachmentState) {
				.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
				.blendEnable = VK_FALSE,
			},
		},
		.pDynamicState = &(VkPipelineDynamicStateCreateInfo){
			.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
			.dynamicStateCount = 2,
			.pDynamicStates = (VkDynamicState[]){
				VK_DYNAMIC_STATE_VIEWPORT,
				VK_DYNAMIC_STATE_SCISSOR,
			},
		},
		.layout = ctx->pipeline_layout,
		.renderPass = ctx->renderpass,
		.subpass = 0,
	}, NULL, &pipeline->graphics_pipeline) != VK_SUCCESS) { return 1; }
	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr = "vk graphics pipeline created"}});

	return 0;
}

static void r_cleanup_pipeline(renderer_context_t *ctx, struct renderer_pipeline *pipeline) {
	vkDestroyPipeline(ctx->device, pipeline->graphics_pipeline, NULL);
	vkDestroyShaderModule(ctx->device, pipeline->shader_frag, NULL);
	vkDestroyShaderModule(ctx->device, pipeline->shader_vert, NULL);
}

static void r_cleanup_stages(renderer_context_t *ctx, uint64_t stage_bits);
static int r_process_cvar_log(renderer_context_t *ctx) {
	assert(ctx->engine_state->cvar_ctx->logger->nevents > 0 && "cvar logger should always have events, since it should be initialised with default cvar events");

	// create a nice array by defining a union type of all r_conf
	// structures
	union {
		r_conf_swapchain_t swapchain;
		r_conf_pipeline_t pipeline;
		r_conf_pipeline_t pipeline_chart;
	} conf[R_CONF_MAX];
	uint64_t changes_bitmap = 0;

	const logger_t *logger = ctx->consumer_cvar->logger;
	uint64_t pos = ctx->consumer_cvar->pos;
	const uint64_t nevents = logger->nevents;
	// see if anything relevant changed
	for (uint64_t i = pos; i < nevents; i++) {
		event_t *ev = &logger->events[i];
		if (ev->type == EVENT_CVAR_SET) {
			event_cvar_set_t *cvar_set = (event_cvar_set_t *)ev;
			switch (cvar_set->variant) {
				case CVAR_R_VSYNC:
				case CVAR_R_WIDTH:
				case CVAR_R_HEIGHT:
				case CVAR_R_SWAPCHAIN_NIMAGES:
				case CVAR_R_NCONCURRENT_FRAMES:
					changes_bitmap |= 1 << R_CONF_SWAPCHAIN;
					break;
				case CVAR_R_VERT:
				case CVAR_R_FRAG:
					changes_bitmap |= 1 << R_CONF_PIPELINE;
					break;
				case CVAR_R_CHART_VERT:
				case CVAR_R_CHART_FRAG:
					changes_bitmap |= 1 << R_CONF_PIPELINE_CHART;
					break;
				default:
					break;
			}
		}
	}
	ctx->consumer_cvar->pos = nevents;

	if (!changes_bitmap) {
		// TODO this should never be the case right after starting.
		// There's an assert in place to check nevents before calling
		// r_prepare, but the events in the log should specifically be
		// of type cvar_set
		return 0;
	}

	// build the configs by looping through all the history
	r_conf_swapchain_t *swapchain = &conf[R_CONF_SWAPCHAIN].swapchain;
	r_conf_pipeline_t *pipeline = &conf[R_CONF_PIPELINE].pipeline;
	r_conf_pipeline_t *pipeline_chart = &conf[R_CONF_PIPELINE_CHART].pipeline_chart;
	for (uint64_t i = 0; i < nevents; i++) {
		event_cvar_set_t *cvar_set = (event_cvar_set_t *)&logger->events[i];
		if (cvar_set->type != EVENT_CVAR_SET) { continue; }
		switch (cvar_set->variant) {
			case CVAR_R_VSYNC: swapchain->vsync = (_Bool)cvar_set->numeric;
				break;
			case CVAR_R_WIDTH: swapchain->width = cvar_set->numeric;
				break;
			case CVAR_R_HEIGHT: swapchain->height = cvar_set->numeric;
				break;
			case CVAR_R_SWAPCHAIN_NIMAGES:
				if (cvar_set->numeric > ctx->surface_capabilities.maxImageCount) { break; }
				if (cvar_set->numeric < ctx->surface_capabilities.minImageCount) { break; }
				swapchain->nimages = cvar_set->numeric;
				break;
			case CVAR_R_NCONCURRENT_FRAMES: swapchain->nconcurrent_frames = cvar_set->numeric;
				break;
			// TODO: when processing more than 1 each of
			// r_vert or r_frag, a shader module for which
			// a cleanup is never triggered is created
			case CVAR_R_VERT: r_prepare_pipeline_shader(ctx, cvar_set->cstr, &pipeline->shader_vert);
				break;
			case CVAR_R_FRAG: r_prepare_pipeline_shader(ctx, cvar_set->cstr, &pipeline->shader_frag);
				break;
			case CVAR_R_CHART_VERT: r_prepare_pipeline_shader(ctx, cvar_set->cstr, &pipeline_chart->shader_vert);
				break;
			case CVAR_R_CHART_FRAG: r_prepare_pipeline_shader(ctx, cvar_set->cstr, &pipeline_chart->shader_frag);
				break;
			default:
				break;
		}
	}

	// Only clean up those that were saved as initialised. On the
	// first run the and results in 0
	r_cleanup_stages(ctx, changes_bitmap & ctx->stages_initialised);
	ctx->stages_initialised |= changes_bitmap;
	// these should be kept in the same order as in enum r_conf_type
	if (changes_bitmap & (1 << R_CONF_SWAPCHAIN)) {
		swapchain->surface_transform = ctx->surface_capabilities.currentTransform;
		if (r_prepare_swapchain(ctx, swapchain)) { return 1; }
		gui_set_dims(ctx->engine_state->gui_ctx, &(gui_dims_t) {
			{ -1.0f, -1.0f },
			{  1.0f,  1.0f },
			{  0.0f,  0.0f },
			{ (float)ctx->swapchain_extent.width, (float)ctx->swapchain_extent.height }
		});
	}
	if (changes_bitmap & ((1 << R_CONF_PIPELINE) | (1 << R_CONF_SWAPCHAIN))) {
		if (r_prepare_pipeline(ctx, &ctx->pipeline, pipeline)) { return 1; }
	}
	if (changes_bitmap & ((1 << R_CONF_PIPELINE_CHART) | (1 << R_CONF_SWAPCHAIN))) {
		if (r_prepare_pipeline(ctx, &ctx->pipeline_chart, pipeline_chart)) { return 1; }
	}
	return 0;
}

static renderer_context_t *r_prepare(engine_state_t *engine_state) {
	renderer_context_t *ctx = mem_push(engine_state->mem, sizeof(*ctx));
	{
		renderer_context_t c = {
			.engine_state = engine_state,
			.logger = logger_create(engine_state->mem, engine_state->reference_time,"r"),
			.consumer_cvar = log_consumer_create(engine_state->mem, engine_state->cvar_ctx->logger),
			.consumer_ri = log_consumer_create(engine_state->mem, engine_state->ri_ctx->logger),
		};
		loader_memcpy(ctx, &c, sizeof(*ctx));
	}
	return ctx;
}

static int r_draw_immediate(VkCommandBuffer buf, renderer_context_t *ctx) {
	vec2 *vertices;
	if (vkMapMemory(ctx->device, ctx->mem_vbuf, 0, ctx->mem_vbufsz, 0, (void **)&vertices) != VK_SUCCESS) { return 1; }

	const uint64_t nevents = ctx->consumer_ri->logger->nevents;
	// p_verts points to the first vertex of the run that uses the active
	// material. The pointer is moved when material.pipeline changes.
	vec2 *p_verts = vertices;
	uint64_t skip_vertices = 0;

	// TODO: how might I source the vertexbuffers?
	vkCmdBindVertexBuffers2(buf, 0, 1, &(VkBuffer){ ctx->vbuf }, &(VkDeviceSize){ 0 }, NULL, NULL);
	for (uint64_t i = ctx->consumer_ri->pos; i < nevents; i++) {
		event_ri_mesh_t *mesh = &ctx->engine_state->ri_ctx->logger->events[i].ri_mesh;
		event_ri_material_t *material = &ctx->engine_state->ri_ctx->logger->events[i].ri_material;
		const enum event_type type = mesh->type;
		if (type == EVENT_RI_MESH) {
			loader_memcpy(p_verts, mesh->vertices, mesh->nvertices * sizeof(*mesh->vertices));
			p_verts += mesh->nvertices;
		}

		const uint32_t nvertices = p_verts - vertices;
		if ((nvertices != 0 && type == EVENT_RI_MATERIAL) || i == nevents - 1) {
			// The last used pipeline is not automatically carried
			// over, and a bind must be included in the command
			// buffer. Even if we checked against current_pipeline
			// != null, we violate spec VUID-vkCmdDraw-None-02700.
			vkCmdDraw(buf, nvertices - skip_vertices, 1, skip_vertices, 0);
			skip_vertices = nvertices;
		}

		if (type == EVENT_RI_MATERIAL) {
			vkCmdBindPipeline(buf, VK_PIPELINE_BIND_POINT_GRAPHICS, material->pipeline);
		}
	}
	ctx->consumer_ri->pos = nevents;
	// unmapping here should be fine, since the commandbuffer will be
	// queued for a while after this function exits
	vkUnmapMemory(ctx->device, ctx->mem_vbuf);
	return 0;
}


static int r_frame_commandbuffer_record(renderer_context_t *ctx, uint32_t swapchain_image_idx) {
	VkCommandBuffer buf = ctx->commandbuffers[ctx->frame_num];
	if (vkBeginCommandBuffer(buf, &(VkCommandBufferBeginInfo){
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
	}) != VK_SUCCESS) { return 1; }

	vkCmdBeginRenderPass2(buf, &(VkRenderPassBeginInfo){
		.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
		.renderPass = ctx->renderpass,
		.framebuffer = ctx->framebuffers[swapchain_image_idx],
		.renderArea = {
			.offset = { 0, 0 },
			.extent = ctx->swapchain_extent,
		},
		.clearValueCount = 1,
		.pClearValues = (VkClearValue[]){
			{{{ 0.0f, 0.0f, 0.0f, 1.0f }}},
		},
	}, &(VkSubpassBeginInfo){
		.sType = VK_STRUCTURE_TYPE_SUBPASS_BEGIN_INFO,
		.contents = VK_SUBPASS_CONTENTS_INLINE,
	});

	vkCmdSetViewport(buf, 0, 1, &(VkViewport){
		.x = 0.0f,
		.y = 0.0f,
		.width = ctx->swapchain_extent.width,
		.height = ctx->swapchain_extent.height,
		.minDepth = 0.0f,
		.maxDepth = 1.0f,
	});
	vkCmdSetScissor(buf, 0, 1, &(VkRect2D){
		.offset = { 0, 0 },
		.extent = ctx->swapchain_extent,
	});

	if (r_draw_immediate(buf, ctx)) { return 1; }

	vkCmdEndRenderPass2(buf, &(VkSubpassEndInfo){ VK_STRUCTURE_TYPE_SUBPASS_END_INFO, 0 });
	if (vkEndCommandBuffer(buf) != VK_SUCCESS) { return 1; }
	return 0;
}

static int r_draw(renderer_context_t *ctx) {
	uint32_t swapchain_image_idx;
	DBG(ctx->logger, &(event_t){.test = {EVENT_TEST, .cstr="begin frame"}});
	vkAcquireNextImageKHR(ctx->device, ctx->swapchain, UINT64_MAX, ctx->sems_image_available[ctx->frame_num], VK_NULL_HANDLE, &swapchain_image_idx);

	vkResetCommandBuffer(ctx->commandbuffers[ctx->frame_num], 0);
	if (r_frame_commandbuffer_record(ctx, swapchain_image_idx)) { return 1; }

	if (vkQueueSubmit2(ctx->q_graphics, 1, &(VkSubmitInfo2){
		.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2,
		.waitSemaphoreInfoCount = 1,
		.pWaitSemaphoreInfos = &(VkSemaphoreSubmitInfo){
			.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO,
			.semaphore = ctx->sems_image_available[ctx->frame_num],
			.stageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
		},
		.commandBufferInfoCount = 1,
		.pCommandBufferInfos = &(VkCommandBufferSubmitInfo){
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO,
			.commandBuffer = ctx->commandbuffers[ctx->frame_num],
		},
		.signalSemaphoreInfoCount = 1,
		.pSignalSemaphoreInfos = &(VkSemaphoreSubmitInfo){
			.sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO,
			.semaphore = ctx->sems_render_finished[ctx->frame_num],
			.stageMask = VK_PIPELINE_STAGE_2_ALL_GRAPHICS_BIT,
		},
	}, ctx->fens_inflight[ctx->frame_num]) != VK_SUCCESS) { return 1; }

	if (vkQueuePresentKHR(ctx->q_present, &(VkPresentInfoKHR){
		.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
		.waitSemaphoreCount = 1,
		.pWaitSemaphores = (VkSemaphore[]){ ctx->sems_render_finished[ctx->frame_num] },
		.swapchainCount = 1,
		.pSwapchains = (VkSwapchainKHR[]){ ctx->swapchain },
		.pImageIndices = (uint32_t[]){ swapchain_image_idx },
	}) != VK_SUCCESS) { return 1; };
	return 0;
}

static int r_frame_prepare(renderer_context_t *ctx) {
	vkWaitForFences(ctx->device, 1, &ctx->fens_inflight[ctx->frame_num], VK_TRUE, UINT64_MAX);
	vkResetFences(ctx->device, 1, &ctx->fens_inflight[ctx->frame_num]);
	r_draw(ctx);
	ctx->frame_num = (ctx->frame_num + 1) % ctx->nconcurrent_frames;
	return 0;
}

static void r_cleanup_swapchain(renderer_context_t *ctx) {
	for (uint32_t i = 0; i < ctx->nconcurrent_frames; i++) {
		vkDestroyFence(ctx->device, ctx->fens_inflight[i], NULL);
		vkDestroySemaphore(ctx->device, ctx->sems_render_finished[i], NULL);
		vkDestroySemaphore(ctx->device, ctx->sems_image_available[i], NULL);
	}
	vkFreeCommandBuffers(ctx->device, ctx->commandpool, ctx->nconcurrent_frames, ctx->commandbuffers);
	vkDestroyCommandPool(ctx->device, ctx->commandpool, NULL);
	for (uint32_t i = 0; i < ctx->swapchain_nimages; i++) {
		vkDestroyFramebuffer(ctx->device, ctx->framebuffers[i], NULL);
	}
	vkDestroyRenderPass(ctx->device, ctx->renderpass, NULL);
	for (uint32_t i = 0; i < ctx->swapchain_nimages; i++) {
		vkDestroyImageView(ctx->device, ctx->imview_swapchain[i], NULL);
	}
	vkDestroySwapchainKHR(ctx->device, ctx->swapchain, NULL);
	// im_swapchain is allocated to engine_state->mem
	mem_return(ctx->engine_state->mem, ctx->fens_inflight, ctx->nconcurrent_frames * sizeof(*ctx->fens_inflight));
	mem_return(ctx->engine_state->mem, ctx->sems_render_finished, ctx->nconcurrent_frames * sizeof(*ctx->sems_render_finished));
	mem_return(ctx->engine_state->mem, ctx->sems_image_available, ctx->nconcurrent_frames * sizeof(*ctx->sems_image_available));
	mem_return(ctx->engine_state->mem, ctx->commandbuffers, ctx->nconcurrent_frames * sizeof(*ctx->commandbuffers));
	mem_return(ctx->engine_state->mem, ctx->framebuffers, ctx->swapchain_nimages * sizeof(*ctx->framebuffers));
	mem_return(ctx->engine_state->mem, ctx->imview_swapchain, ctx->swapchain_nimages * sizeof(*ctx->imview_swapchain));
	mem_return(ctx->engine_state->mem, ctx->im_swapchain, ctx->swapchain_nimages * sizeof(*ctx->im_swapchain));
}

static void r_cleanup_base(renderer_context_t *ctx) {
	vkDestroyPipelineLayout(ctx->device, ctx->pipeline_layout, NULL);
	vkDestroyBuffer(ctx->device, ctx->vbuf, NULL);
	vkFreeMemory(ctx->device, ctx->mem_vbuf, NULL);
	vkDestroySurfaceKHR(ctx->instance, ctx->surface, NULL);
	vkDestroyDevice(ctx->device, NULL);
	vkDestroyInstance(ctx->instance, NULL);
}

static void r_cleanup_stages(renderer_context_t *ctx, uint64_t stage_bits) {
	if (stage_bits) { vkDeviceWaitIdle(ctx->device); }
	if (stage_bits & (1 << R_CONF_PIPELINE_CHART)) { r_cleanup_pipeline(ctx, &ctx->pipeline_chart); }
	if (stage_bits & (1 << R_CONF_PIPELINE)) { r_cleanup_pipeline(ctx, &ctx->pipeline); }
	if (stage_bits & (1 << R_CONF_SWAPCHAIN)) { r_cleanup_swapchain(ctx); }
}

static void r_cleanup(renderer_context_t *ctx) {
	r_cleanup_stages(ctx, UINT64_MAX);
	r_cleanup_base(ctx);
	log_consumer_destroy(ctx->engine_state->mem, ctx->consumer_ri);
	log_consumer_destroy(ctx->engine_state->mem, ctx->consumer_cvar);
	logger_destroy(ctx->engine_state->mem, ctx->logger);
	mem_return(ctx->engine_state->mem, ctx, sizeof(*ctx));
}

static void guicb_rect(void *data, vec2 pos, vec2 size) {
	ri_context_t *ctx = data;
	vec2 *rect = mem_push(ctx->engine_state->mem, sizeof(vec2) * 6);
	glm_vec2_copy((vec2){ pos[0], pos[1] }, rect[0]);
	glm_vec2_copy((vec2){ size[0] + pos[0], size[1] + pos[1] }, rect[1]);
	glm_vec2_copy((vec2){ pos[0], size[1] + pos[1] }, rect[2]);
	glm_vec2_copy((vec2){ pos[0], pos[1] }, rect[3]);
	glm_vec2_copy((vec2){ size[0] + pos[0], pos[1] }, rect[4]);
	glm_vec2_copy((vec2){ size[0] + pos[0], size[1] + pos[1] }, rect[5]);
	logev(ctx->logger, &(event_t){.ri_mesh = {EVENT_RI_MESH, .vertices = rect, .nvertices = 6}});
}

static void guicb_line(void *data, vec2 p0, vec2 p1, float width) {
	ri_context_t *ctx = data;
	vec2 normal0 = { p1[1] - p0[1], p0[0] - p1[0] };
	glm_vec2_normalize(normal0);

	width *= 0.5f;
	glm_vec2_scale(normal0, width, normal0);
	vec2 tl, tr, br, bl;
	glm_vec2_copy((vec2){ p0[0], p0[1] }, tl);
	glm_vec2_copy((vec2){ p1[0], p1[1] }, tr);
	glm_vec2_copy((vec2){ p0[0], p0[1] }, bl);
	glm_vec2_copy((vec2){ p1[0], p1[1] }, br);
	glm_vec2_add(tl, normal0, tl);
	glm_vec2_sub(bl, normal0, bl);
	glm_vec2_add(tr, normal0, tr);
	glm_vec2_sub(br, normal0, br);

	vec2 *line_rect = mem_push(ctx->engine_state->mem, sizeof(vec2) * 6);
	glm_vec2_copy(tl, line_rect[0]);
	glm_vec2_copy(tr, line_rect[1]);
	glm_vec2_copy(bl, line_rect[2]);
	glm_vec2_copy(bl, line_rect[3]);
	glm_vec2_copy(tr, line_rect[4]);
	glm_vec2_copy(br, line_rect[5]);

	logev(ctx->logger, &(event_t){.ri_mesh = {EVENT_RI_MESH, .vertices = line_rect, .nvertices = 6}});
}

static void guicb_linearea(void *data, vec2 p0, vec2 p1) {
	ri_context_t *ctx = data;
	const float midpoint = 0.0f;

	// if we cross the x-axis, draw the area in two parts
	if (p0[1] != midpoint && p1[1] != midpoint && p0[1] > midpoint ^ p1[1] > midpoint) {
		vec2 unit_vector;
		glm_vec2_sub(p1, p0, unit_vector);
		glm_vec2_normalize(unit_vector);

		vec2 to_x_intersect;
		glm_vec2_scale(unit_vector, (midpoint - p0[1]) / unit_vector[1], to_x_intersect);

		glm_vec2_add(to_x_intersect, p0, to_x_intersect);
		// to_x_intersect[1] is pretty much equal to midpoint here, but
		// it is not necessarily exact. Set it here so it can be
		// compared with !=
		to_x_intersect[1] = midpoint;
		guicb_linearea(ctx, (vec2){ p0[0], p0[1] }, to_x_intersect);
		guicb_linearea(ctx, to_x_intersect, (vec2){ p1[0], p1[1] });
		return;
	}

	vec2 tl, tr, br, bl;
	if (p0[1] < midpoint || p1[1] < midpoint) {
		glm_vec2_copy((vec2){ p0[0], p0[1] }, tl);
		glm_vec2_copy((vec2){ p1[0], p1[1] }, tr);
		glm_vec2_copy((vec2){ p0[0], midpoint }, bl);
		glm_vec2_copy((vec2){ p1[0], midpoint }, br);
	} else {
		glm_vec2_copy((vec2){ p0[0], p0[1] }, bl);
		glm_vec2_copy((vec2){ p1[0], p1[1] }, br);
		glm_vec2_copy((vec2){ p0[0], midpoint }, tl);
		glm_vec2_copy((vec2){ p1[0], midpoint }, tr);
	}

	vec2 *rect = mem_push(ctx->engine_state->mem, sizeof(vec2) * 6);
	glm_vec2_copy(tl, rect[0]);
	glm_vec2_copy(tr, rect[1]);
	glm_vec2_copy(bl, rect[2]);
	glm_vec2_copy(bl, rect[3]);
	glm_vec2_copy(tr, rect[4]);
	glm_vec2_copy(br, rect[5]);
	logev(ctx->logger, &(event_t){.ri_mesh = {EVENT_RI_MESH, .vertices = rect, .nvertices = 6}});
}

static void guicb_linejoin_bevel(void *data, vec2 p0, vec2 p1, vec2 p2, vec2 p3, float width) {
	ri_context_t *ctx = data;
	vec2 normal0, normal1;
	glm_vec2_normalize_to((vec2){ p1[1] - p0[1], p0[0] - p1[0] }, normal0);
	glm_vec2_normalize_to((vec2){ p3[1] - p2[1], p2[0] - p3[0] }, normal1);

	width *= 0.5f;
	glm_vec2_scale(normal0, width, normal0);
	glm_vec2_scale(normal1, width, normal1);

	vec2 *join_tri = mem_push(ctx->engine_state->mem, sizeof(vec2) * 3);
	if (glm_vec2_cross(normal0, normal1) > 0) {
		glm_vec2_add(p1, normal0, join_tri[0]);
		glm_vec2_add(p1, normal1, join_tri[1]);
	} else {
		glm_vec2_scale(normal0, -1, normal0);
		glm_vec2_scale(normal1, -1, normal1);
		glm_vec2_add(p1, normal0, join_tri[1]);
		glm_vec2_add(p1, normal1, join_tri[0]);
	}
	glm_vec2_copy(p1, join_tri[2]);
	logev(ctx->logger, &(event_t){.ri_mesh = {EVENT_RI_MESH, .vertices = join_tri, .nvertices = 3}});
}

static int engine_loader_addrs_prepare(pfn_loader_addr loader_addr) {
	int any_errored = 0;
#ifndef NDEBUG
	#define DEBUG_PRINT_ADDR(name) \
		if (loader_printf) { loader_printf("%s: " #name " is at %p\n", __func__, name); }
#else
	#define DEBUG_PRINT_ADDR(name)
#endif

	#define PFN_FORMAT(type, name, args, loadername) \
		name = (pfn_##name)loader_addr(PFN_##name); \
		if (name == NULL) { any_errored = 1; } \
		//DEBUG_PRINT_ADDR(name)
	PFN_LOADER_LIST
	#undef PFN_FORMAT
	// example unwrapping:
	//#undef loader_memcpy
	//loader_memcpy = (pfn_loader_memcpy)loader_addr(PFN_loader_memcpy);

	if (any_errored) {
		if (loader_printf == NULL) { return 1; }
		// NOTE: this is likely to happen if loader PFNs are changed without
		// restarting the loader
		loader_printf("%s: some pfns were null\n", __func__);
		return 1;
	}
	return 0;
}

#if 0
static void engine_loader_addrs_print() {
	#define PFN_FORMAT(type, name, args, loadername) \
		#type " " #name " " #args "\0"
	const char pfns[] = PFN_LOADER_LIST;
	#undef PFN_FORMAT
	char *p;
	for (p = (char*)pfns; p <= pfns + sizeof(pfns);) {
		loader_printf("%s\n", p);
		p += loader_strlen(p) + 1;
	}
}
#endif

static engine_state_t *engine_prepare(persist_state_t *persist_state) {
	timestamp_t reference;
	loader_timestamp(&reference.sec, &reference.nsec);

	const uint64_t memsz = (uint64_t)1 << 32;
	mem_t *mem = loader_mem(memsz);
	if (mem == NULL) { return NULL; }
	loader_memcpy(mem, &(mem_t){
		.sz = 0,
	}, sizeof(mem_t));
	engine_state_t *state = mem_push(mem, sizeof(*state));
	persist_state->engine_state = state;
	state->memsz = memsz;
	state->mem = mem;

	state->reference_time = reference;
	state->engine_logger = logger_create(state->mem, state->reference_time, "engine");

	DBG(state->engine_logger, &(event_t){.test = {EVENT_TEST, .cstr = "state allocated"}});

	state->cvar_ctx = cvar_prepare(state);
	if (!state->cvar_ctx) { return NULL; }

	state->ri_ctx = ri_prepare(state);
	if (!state->ri_ctx) { return NULL; }

	state->consumer_cvar = log_consumer_create(state->mem, state->cvar_ctx->logger);

	state->gui_ctx = mem_push(state->mem, sizeof(gui_context_t));
	*state->gui_ctx = (gui_context_t){
		.userdata = state->ri_ctx,
		.callbacks = mem_push(state->mem, sizeof(gui_callbacks_t)),
		.max_z_index = 0,
		.mem = state->mem,
	};
	state->gui_ctx->root = (gui_node_t *)mem_push(state->mem, sizeof(*state->gui_ctx->root));
	state->gui_ctx->root->ctx = state->gui_ctx;

	cvar_create_default_renderer_events(state->cvar_ctx);
	// allocate our logger before r_prepare, since otherwise we'd need to
	// cut r_cleanup into two. Later set consumer->r_logger to a non-null
	// value.
	state->consumer_r = log_consumer_create(state->mem, NULL);
	state->r_ctx = r_prepare(state);
	if (!state->r_ctx) { return NULL; }
	state->consumer_r->logger = state->r_ctx->logger;

	// renderer reads cvar log to (re)build and apply its configuration.
	// This creates the initial events to configure renderer.
	if (
		r_prepare_base(state->r_ctx)
		|| r_process_cvar_log(state->r_ctx)
		// r_prepare_swapchain shortcut
		// r_prepare_pipeline shortcut
	) {
		log_print(state->r_ctx->logger, 0, NULL);
		return NULL;
	}
	return state;
}

static void engine_cleanup(engine_state_t *state) {
	r_cleanup(state->r_ctx);
	log_consumer_destroy(state->mem, state->consumer_r);
	mem_return(state->mem, state->gui_ctx->root, sizeof(*state->gui_ctx->root));
	mem_return(state->mem, state->gui_ctx->callbacks, sizeof(gui_callbacks_t)),
	mem_return(state->mem, state->gui_ctx, sizeof(gui_context_t));
	log_consumer_destroy(state->mem, state->consumer_cvar);
	ri_cleanup(state->mem, state->ri_ctx);
	cvar_cleanup(state->mem, state->cvar_ctx);
	logger_destroy(state->mem, state->engine_logger);
	//mem_return(state->mem, state, sizeof(*state));
	loader_mem_free(state->mem, state->memsz);
}

enum ui_type {
	UI_TYPE_DRAGGABLE,
	UI_TYPE_SELECT,
};

typedef union ui {
	enum ui_type type;
	struct {
		enum ui_type type;
		vec2 pos;
	} draggable;
	struct {
		enum ui_type type;
		vec2 pos;
		vec2 size;
	} select;
} ui_t;

static ui_t *ui_find_focused(gui_node_t *root, vec2 pointer) {
	gui_node_t *focused = NULL;
	for (gui_node_t *n = root->first_child; n != NULL; n = n->next) {
		if (pointer[0] < n->outbounds[0][0]) { continue; }
		if (pointer[1] < n->outbounds[0][1]) { continue; }
		if (pointer[0] > n->outbounds[1][0]) { continue; }
		if (pointer[1] > n->outbounds[1][1]) { continue; }
		if (focused && focused->z_index > n->z_index) { continue; }
		focused = n;
	}

	if (!focused) { return NULL; }
	return focused->userdata;
}

int engine_entrypoint(persist_state_t *persist_state, pfn_loader_addr loader_addr) {
	if (engine_loader_addrs_prepare(loader_addr)) { return 1; }
	engine_state_t *state = persist_state->engine_state;
	if (!state) {
		state = engine_prepare(persist_state);
		if (!state) { return 1; }
		persist_state->engine_state = state;
	} else {
		// our globals' lifetimes end when the dlib is freed, resolve
		// them again here
		if (r_vulkan_proc_addrs_prepare()) { return 1; }
		if (r_vulkan_proc_addrs_resolve(state->r_ctx)) { return 1; }
		// issue recreation of renderer stages with a cvar_set
		//state->r_ctx->consumer_cvar->pos = 0;
		// for the time being, prefer recreating the events instead,
		// since there is currently no other way to trigger changes to
		// the values than editing the default events
		cvar_create_default_renderer_events(state->cvar_ctx);
	}
	state->nentries++;

	loader_printf("dlib! nentries: %d\n", state->nentries);

	DBG(state->engine_logger, &(event_t){.test = {EVENT_TEST, .cstr="state prepared/reloaded"}});

	log_print(state->engine_logger, 0, NULL);
	log_print(state->cvar_ctx->logger, state->consumer_cvar->pos, &state->consumer_cvar->pos);

	ui_t *panel = &(ui_t){.draggable = {UI_TYPE_DRAGGABLE, {100.0f, 100.0f}}};
	ui_t *panel2 = &(ui_t){.draggable = {UI_TYPE_DRAGGABLE, {100.0f, 100.0f}}};
	ui_t *select_rect = &(ui_t){.select = {UI_TYPE_SELECT, GLM_VEC2_ZERO_INIT, GLM_VEC2_ZERO_INIT}};

	ui_t *ui_dragged = NULL;

	*state->gui_ctx->callbacks = (gui_callbacks_t){
		.rect = guicb_rect,
	};

	const double *pointer_ = loader_pointer_position();
	vec2 pointer_old = GLM_VEC2_ZERO_INIT;
	uint8_t running = 1;

	uint64_t frames_drawn = 0;
	while (running) {
		if (loader_dlib_update_available()) {
			persist_state->engine_exit_reason = ENGINE_EXIT_RELOAD;
			return 0;
		}
		log_print(state->r_ctx->logger, state->consumer_r->pos, &state->consumer_r->pos);
		vec2 pointer = { pointer_[0], pointer_[1] };
		vec2 dpointer;
		glm_vec2_sub(pointer, pointer_old, dpointer);

		void *frame_checkpoint = mem_push(state->mem, 0);

		const uint32_t nvalues = 15;
		float *values = mem_push(state->mem, sizeof(float[nvalues]));
		for (uint32_t i = 0; i < nvalues; i++) {
			values[i] = sin(2 * CGLM_PI * i * 0.066f + frames_drawn * 0.001f) * 0.66f;
		}

		logev(state->ri_ctx->logger, &(event_t){.ri_material = {EVENT_RI_MATERIAL, .pipeline = state->r_ctx->pipeline_chart.graphics_pipeline}});
		const float step_x = 0.06f;
		const float line_width = 0.05f;
		vec2 p0, p1, old0, old1;
		for (uint32_t i = 0; i < nvalues - 1; i++) {
			glm_vec2_copy(p0, old0);
			glm_vec2_copy(p1, old1);
			glm_vec2_copy((vec2){ step_x * i, values[i] }, p0);
			glm_vec2_copy((vec2){ step_x * (i+1), values[i+1] }, p1);
			guicb_linearea(state->ri_ctx, p0, p1);
		}
		logev(state->ri_ctx->logger, &(event_t){.ri_material = {EVENT_RI_MATERIAL, .pipeline = state->r_ctx->pipeline.graphics_pipeline}});
		for (uint32_t i = 0; i < nvalues - 1; i++) {
			glm_vec2_copy(p0, old0);
			glm_vec2_copy(p1, old1);
			glm_vec2_copy((vec2){ step_x * i, values[i] }, p0);
			glm_vec2_copy((vec2){ step_x * (i+1), values[i+1] }, p1);
			guicb_line(state->ri_ctx, p0, p1, line_width);
			if (i != 0) {
				guicb_linejoin_bevel(state->ri_ctx, old0, old1, p0, p1, line_width);
			}
		}

		logev(state->ri_ctx->logger, &(event_t){.ri_material = {EVENT_RI_MATERIAL, .pipeline = state->r_ctx->pipeline.graphics_pipeline}});
		gui_node_t *gui_root = state->gui_ctx->root;
		gui_titled_panel(gui_root, panel->draggable.pos, (vec2){50.0f, 50.0f}, panel);
		gui_titled_panel(gui_root, panel2->draggable.pos, (vec2){20.0f, 20.0f}, panel2);
		gui_titled_panel(gui_root, select_rect->select.pos, select_rect->select.size, select_rect);
		logev(state->ri_ctx->logger, &(event_t){.ri_material = {EVENT_RI_MATERIAL, .pipeline = state->r_ctx->pipeline_chart.graphics_pipeline}});

		ui_t *ui_hovered = ui_find_focused(gui_root, pointer);
		gui_root->first_child = NULL;

		char *buf = mem_push(state->mem, 4096);
		const uint64_t nbytes = loader_inputs(buf, mem_grown_since(state->mem, buf));
		const input_state_t *ev;
		for (char *p = buf; p < buf + nbytes; p += sizeof(*ev)) {
			ev = (input_state_t *)p;
			if (!ev->direction_down) {
				switch (ev->input) {
					case INPUT_MOUSE1:
						if (ui_dragged == select_rect) {
							// TODO should the
							// check be for the
							// exact pointer, or
							// the type?
							glm_vec2_zero(select_rect->select.size);
						}
						ui_dragged = NULL;
						break;
					default:
						break;
				}
				continue; // next switch-case is for input down only
			}
			switch (ev->input) {
				case 'Q':
				case 'q':
					running = 0;
					break;
				case INPUT_MOUSE1:
					ui_dragged = ui_hovered;
					if (!ui_dragged) {
						glm_vec2_copy(pointer, select_rect->select.pos);
						ui_dragged = select_rect;
					}
					// TODO perhaps bring z-index to front when clicked
					loader_printf("%f %f\n", pointer[0], pointer[1]);
					break;
				default:
					break;
			};
		}

		if (ui_dragged) {
			switch (ui_dragged->type) {
				case UI_TYPE_DRAGGABLE:
					glm_vec2_add(ui_dragged->draggable.pos, dpointer, ui_dragged->draggable.pos);
					break;
				case UI_TYPE_SELECT:
					glm_vec2_add(ui_dragged->select.size, dpointer, ui_dragged->select.size);
					break;
				default:
					break;
			}
		}

		if (r_frame_prepare(state->r_ctx)) { return 1; }
		mem_return_unchecked(state->mem, frame_checkpoint);
		loader_wayland_display_dispatch_pending();
		if (r_process_cvar_log(state->r_ctx)) { return 1; }
		glm_vec2_copy(pointer, pointer_old);

		frames_drawn++;
	}

	// since reloads cause an early exit out of this call, we now know that
	// we are quitting
	{
		persist_state->engine_exit_reason = ENGINE_EXIT_QUIT;
		engine_cleanup(state);
	}
	return 0;
}
// engine_prepare shortcut
// r_process_cvar_log shortcut
// r_prepare_pipeline shortcut
// r_frame_prepare shortcut
// r_frame_commandbuffer_record shortcut
