# * either install cglm into /usr/include, in which case the CGLM= environment
#   value may be omitted
# * or symlink into repo: ln -s path-to-cglm cglm~
#   * cglm~/include/cglm should be a valid directory
# toolbox run bash -c 'CGLM=cglm~ make --always-make; echo $? >errcode~'; [ `<errcode~` == 0 ] && (cd target/debug; ASAN_OPTIONS=detect_leaks=0 ./loader; echo "Exit code $?"; cd $OLDPWD)
CC=clang
RELEASE_DIR=target/release
DEBUG_DIR=target/debug
CFLAGS=-std=c2x -fpie -fpic -Wall -Wextra -Wsign-conversion -pedantic -Werror=return-type -Werror
#CFLAGS+=-fsanitize=memory -fsanitize-memory-track-origins
SHADER_SRC=shader.vert white.frag graph_area.frag
SHADER_DST=$(addsuffix .spirv,$(SHADER_SRC))
GLSLCFLAGS=--target-env=vulkan1.2 -Werror

all: debug

release: CFLAGS+=-s -DNDEBUG
release: $(RELEASE_DIR)/ $(RELEASE_DIR)/loader $(addprefix $(RELEASE_DIR)/,$(SHADER_DST)) $(RELEASE_DIR)/engine.so

debug: CFLAGS+=-g -O0
debug: CFLAGS+=-fsanitize=function
debug: CFLAGS+=-fsanitize=address
debug: $(DEBUG_DIR)/ $(DEBUG_DIR)/loader $(addprefix $(DEBUG_DIR)/,$(SHADER_DST)) $(DEBUG_DIR)/engine.so

# dynamic variables, evaluated after the target-specific changes are applied
CFLAGS_ENGINE=$(CFLAGS) -shared -nostartfiles -nostdlib
CFLAGS_ENGINE+=-I "$(if $(CGLM),$(CGLM)/include,)"

%/:
	mkdir -p $@
src/%.h:
# intentionally left empty
%/loader: src/loader.c src/loader.h xdg-shell-protocol.c xdg-shell-client-protocol.h xkbcommon-keysyms.h
	$(CC) $(CFLAGS) -I. -lwayland-client -lxkbcommon -Wl,-rpath=. -o $@ $< xdg-shell-protocol.c
$(RELEASE_DIR)/%.so: src/%.c src/loader.h
	$(CC) $(CFLAGS_ENGINE) -o $@ $<
$(RELEASE_DIR)/%.spirv: src/%
	glslc $(GLSLCFLAGS) -o $@ $<
# couldn't find out how to share these recipes, should be kept the same
$(DEBUG_DIR)/%.so: src/%.c src/loader.h
	$(CC) $(CFLAGS_ENGINE) -o $@ $<
$(DEBUG_DIR)/%.spirv: src/%
	glslc $(GLSLCFLAGS) -o $@ $<
xdg-shell-protocol.c:
	wayland-scanner private-code < /usr/share/wayland-protocols/stable/xdg-shell/xdg-shell.xml > xdg-shell-protocol.c
xdg-shell-client-protocol.h:
	wayland-scanner client-header < /usr/share/wayland-protocols/stable/xdg-shell/xdg-shell.xml > xdg-shell-client-protocol.h
xkbcommon-keysyms.h: keysyms-base.h
	cp $< $@
	sed -E 's/^#define XKB_KEY_([^ \t]+)[ \t]*(.*)/INPUTS_FORMAT(\1, \2) \\/g' /usr/include/xkbcommon/xkbcommon-keysyms.h |grep INPUTS_FORMAT >> "$@"
tags: src/engine.c src/loader.c src/loader.h
	ctags $^

.PHONY: release debug
